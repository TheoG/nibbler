#include "Vector.hpp"

iVector2::iVector2() : x(0), y(0) {}
iVector2::~iVector2() {}
iVector2::iVector2(int x, int y) : x(x), y(y) {}
iVector2::iVector2(const iVector2& v) : x(v.x), y(v.y) {}

iVector2& iVector2::operator+=(const iVector2& v) {
	x += v.x;
	y += v.y;
	return *this;
}
iVector2& iVector2::operator=(const iVector2& v) {
	x = v.x;
	y = v.y;
	return *this;
}

bool iVector2::operator==(const iVector2& other) const {
	return x == other.x && y == other.y; 
}

bool iVector2::operator!=(const iVector2& other) const {
	return !(operator==(other)); 
}
