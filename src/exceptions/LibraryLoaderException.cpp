#include "exceptions/LibraryLoaderException.hpp"

LibraryLoaderException::LibraryLoaderException( void ) {
	_errorMessage = "Can't load that library";
}

LibraryLoaderException::LibraryLoaderException( LibraryLoaderException const &  other) {
	*this = other;
}

LibraryLoaderException::LibraryLoaderException(const std::string& errorMessage) {
	_errorMessage = errorMessage;
}

LibraryLoaderException::~LibraryLoaderException( void ) {
}

LibraryLoaderException & LibraryLoaderException::operator=( LibraryLoaderException const & rhs ) {
	(void) rhs;
	return *this;
}

char const * LibraryLoaderException::what() const throw() {
	return _errorMessage.c_str();
}
