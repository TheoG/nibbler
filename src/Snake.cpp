#include <Snake.hpp>
#include <Game.hpp>

Snake::Snake(iVector2 boardSize, float tickDuration, bool multiplayer): _multiplayer(multiplayer), _tickDuration(tickDuration) {
	++Snake::_nbPlayers;
	_index = Snake::_nbPlayers;
	reset(boardSize, Snake::_nbPlayers);
}

Snake::~Snake() {}

Snake& Snake::operator=(const Snake& other) {
	_multiplayer = other._multiplayer;
	_tickDuration = other._tickDuration;
	_index = other._index;
	reset(iVector2(10, 10), _index);
	return *this;
}

Snake::Snake(const Snake& other) {
	*this = other;
}

bool Snake::moveForward(std::shared_ptr<Board> board) {

	_direction = _nextDirection;
	iVector2 newHeadPosition = _bodyParts.front();

	switch (_direction) {
	case North:
		newHeadPosition += iVector2(0, -1);
		break;
	case East:
		newHeadPosition += iVector2(1, 0);
		break;
	case South:
		newHeadPosition += iVector2(0, 1);
		break;
	case West:
		newHeadPosition += iVector2(-1, 0);
		break;
	}

	// for each body part, move forward
	for (int i = _size - 1; i > 0; --i) {
		_bodyParts[i] = _bodyParts[i - 1];
	}

	if (_hasEaten) {
		_hasEaten = false;
		++_size;
	}

	*_bodyParts.begin() = newHeadPosition;

	Block blockAtHead = board->getBlockAt(newHeadPosition);

	if (blockAtHead == Block::Wall || blockAtHead == Block::Snake_P1 || blockAtHead == Block::Snake_P2 || blockAtHead == Block::Snake_Head_P1 || blockAtHead == Block::Snake_Head_P2) {
		return false;
	}
	return true;
}

bool Snake::update(double elapsedTime) {
	bool shouldSetTickIndex = false;
	if ((_speedUp && _speedBonusTimeStamp < 0) || (_speedDown && _speedBonusTimeStamp < 0)) {
		_speedBonusTimeStamp = elapsedTime;
		shouldSetTickIndex = true;
	}
	if (_speedBonusTimeStamp > 0.0 && elapsedTime - _speedBonusTimeStamp > 5.0) {
		_speedUp = _speedDown = false;
		_speedBonusTimeStamp = -1.0;
		shouldSetTickIndex = true;
	}
	float localTickDuration = _tickDuration;
	if (_speedDown) {
		localTickDuration *= 2;
	} else if (_speedUp) {
		localTickDuration /= 2;
	}
	int tickIndex = static_cast<int>(elapsedTime / localTickDuration);
	if (shouldSetTickIndex) {
		_tickIndex = tickIndex;
	}
	if (_tickIndex != tickIndex) {
		_tickIndex = tickIndex;
		return true;
	}
	return false;
}

void Snake::eat() {
	_bodyParts.push_back(*std::prev(_bodyParts.end()));
	_hasEaten = true;
	increaseScore(10);
}

void Snake::bonus() {

	_bonus = static_cast<Bonuses>((std::rand() + 1) % BonusEnd);

	if (_bonus == Bonuses::SpeedDown) {
		_speedDown = true;
		_speedUp = false;
	} else if (_bonus == Bonuses::SizeDown) {
		if (_bodyParts.size() > 3)
			_bodyParts.pop_back();
	} else if (_bonus == Bonuses::ScoreUp) {
		increaseScore(50);
	} else if (_bonus == Bonuses::SpeedUp) {
		_speedDown = false;
		_speedUp = true;
	} else if (_bonus == Bonuses::SizeUp) {
		_bodyParts.push_back(*std::prev(_bodyParts.end()));
		_hasEaten = true;
	} else if (_bonus == Bonuses::ScoreDown) {
		increaseScore(-50);
	}
}

void Snake::increaseScore(int value) {
	_score = std::clamp(_score + value, 0, 9999);
}

void Snake::reset(iVector2 boardSize, int player) {
	_bodyParts.clear();

	if (!_multiplayer) {
		_bodyParts.push_back(iVector2(boardSize.x / 2, boardSize.y / 2));
		_bodyParts.push_back(iVector2(boardSize.x / 2 - 1, boardSize.y / 2));
		_bodyParts.push_back(iVector2(boardSize.x / 2 - 2, boardSize.y / 2));
		_bodyParts.push_back(iVector2(boardSize.x / 2 - 3, boardSize.y / 2));

		_nextDirection = _direction = Direction::East;
	} else if (_multiplayer && player == 1) {
		_bodyParts.push_back(iVector2(5, 2));
		_bodyParts.push_back(iVector2(4, 2));
		_bodyParts.push_back(iVector2(3, 2));
		_bodyParts.push_back(iVector2(2, 2));

		_nextDirection = _direction = Direction::East;
	} else if (_multiplayer && player == 2) {
		_bodyParts.push_back(iVector2(boardSize.x - 6, boardSize.y - 3));
		_bodyParts.push_back(iVector2(boardSize.x - 5, boardSize.y - 3));
		_bodyParts.push_back(iVector2(boardSize.x - 4, boardSize.y - 3));
		_bodyParts.push_back(iVector2(boardSize.x - 3, boardSize.y - 3));

		_nextDirection = _direction = Direction::West;
	}

	_size = _bodyParts.size();
	_hasEaten = false;
	_playerBlock = player == 1 ? Block::Snake_P1 : Block::Snake_P2;
	_alive = true;
	_score = 0;
	_ready = false;
	_tickIndex = 0;
	_speedDown = _speedUp = false;
	_speedBonusTimeStamp = -1.0;
	_tickDuration = Game::DefaultTickDuration;
}



// Getters
const std::vector<iVector2>& Snake::getBodyParts() const {
	return _bodyParts;
}

const iVector2 Snake::getHeadPosition() const {
	return *_bodyParts.begin();
}

Direction Snake::getNextDirection() const {
	return _nextDirection;
}

Direction Snake::getDirection() const {
	return _direction;
}

Block Snake::getPlayerBlock() const {
	return _playerBlock;
}

int Snake::getTickDuration() const {
	return _tickDuration;
}

int Snake::getScore() const {
	return _score;
}

int Snake::getIndex() const {
	return _index;
}

int Snake::getSize() const {
	return _size;
}

bool Snake::isAlive() const {
	return _alive;
}

bool Snake::isReady() const {
	return _ready;
}

// Setters
void Snake::setBodyParts(std::vector<iVector2>& bodyParts) {
	_bodyParts = bodyParts;
}

void Snake::setTickDuration(float tickDuration) {
	_tickDuration = tickDuration;
}

void Snake::setNextDirection(Direction d) {
	_nextDirection = d;
}

void Snake::setDirection(Direction d) {
	_direction = d;
}

void Snake::setAlive(bool alive) {
	_alive = alive;
}


void Snake::setScore(int score) {
	_score = score;
}

void Snake::setReady(bool ready) {
	_ready = ready;
}

int Snake::_nbPlayers = 0;
