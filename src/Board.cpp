#include "Board.hpp"

Board::Board() {}

Board::Board(iVector2 gameAreaSize, int level): _gameAreaSize(gameAreaSize), _level(level) {
	reset();
}

Board::Board(const Board& other) {
	*this = other;
}

Board& Board::operator=(const Board& other) {
	_gameAreaSize = other._gameAreaSize;
	_level = other._level;
	reset();
	return *this;
}

Board::~Board() {}

void Board::updateSnakePosition(const std::vector<iVector2>& bodyParts) {

	for (int y = 0; y < _gameAreaSize.y; ++y) {
		for (int x = 0; x < _gameAreaSize.x; ++x) {
			if (getBlockAt(x, y) == Block::Snake_P1 || getBlockAt(x, y) == Block::Snake_P2) {
				setBlockAt(x, y, Block::Empty);
			}
		}
	}

	for (auto bodyPart : bodyParts) {
		setBlockAt(bodyPart, Block::Snake_P1);
	}
	setBlockAt(bodyParts.front(), Block::Snake_Head_P1);

	_emptyCells.clear();
	for (int y = 0; y < _gameAreaSize.y; ++y) {
		for (int x = 0; x < _gameAreaSize.x; ++x) {
			if (getBlockAt(x, y) == Block::Empty) {
				_emptyCells.push_back(iVector2(x, y));
			}
		}
	}
}

void Board::updateSnakesPositions(const std::vector<iVector2>& bodyPartsP1, const std::vector<iVector2>& bodyPartsP2) {

	for (int y = 0; y < _gameAreaSize.y; ++y) {
		for (int x = 0; x < _gameAreaSize.x; ++x) {
			if (getBlockAt(x, y) == Block::Snake_P1 || getBlockAt(x, y) == Block::Snake_P2) {
				setBlockAt(x, y, Block::Empty);
			}
		}
	}

	for (auto bodyPart : bodyPartsP1) {
		setBlockAt(bodyPart, Block::Snake_P1);
	}
	setBlockAt(bodyPartsP1.front(), Block::Snake_Head_P1);

	for (auto bodyPart : bodyPartsP2) {
		setBlockAt(bodyPart, Block::Snake_P2);
	}
	setBlockAt(bodyPartsP2.front(), Block::Snake_Head_P2);

	_emptyCells.clear();
	for (int y = 0; y < _gameAreaSize.y; ++y) {
		for (int x = 0; x < _gameAreaSize.x; ++x) {
			if (getBlockAt(x, y) == Block::Empty) {
				_emptyCells.push_back(iVector2(x, y));
			}
		}
	}
}

iVector2 Board::generateFoodAtRandomLocation() {

	if (_emptyCells.size() == 0) {
		return iVector2();
	}
	iVector2 position = _emptyCells[(std::rand() + 1) % _emptyCells.size()];
	setBlockAt(position, Food);
	_emptyCells.erase(std::find(_emptyCells.begin(), _emptyCells.end(), position));
	_foodPosition = position;
	return position;
}

iVector2 Board::generateBonusAtRandomLocation() {

	if (_emptyCells.size() == 0) {
		return iVector2();
	}

	iVector2 position = _emptyCells[(std::rand() + 1) % _emptyCells.size()];
	setBlockAt(position, Block::Bonus);
	_emptyCells.erase(std::find(_emptyCells.begin(), _emptyCells.end(), position));
	_bonusPosition = position;
	_bonusSpawned = true;
	_bonusSpawnTime = std::chrono::high_resolution_clock::now();	
	_bonusLifeTime = (Board::_MaxBonusTime - Board::_MinBonusTime) * (static_cast<double>(std::rand()) / static_cast<double>(RAND_MAX)) + Board::_MinBonusTime;
	return position;

}

bool Board::updateBonus() {

	if (!_bonusSpawned && std::chrono::duration_cast<std::chrono::duration<int>>(std::chrono::high_resolution_clock::now() - _bonusUnspawnTime).count() > _nextBonusTime) {
		generateBonusAtRandomLocation();
		return true;
	}
	if (_bonusSpawned && std::chrono::duration_cast<std::chrono::duration<int>>(std::chrono::high_resolution_clock::now() - _bonusSpawnTime).count() > _bonusLifeTime) {
		unspawnBonus();
		return true;
	}
	return false;
}

void Board::unspawnBonus() {
	for (int y = 0; y < _gameAreaSize.y; y++) {
		for (int x = 0; x < _gameAreaSize.x; x++) {
			if (getBlockAt(x, y) == Block::Bonus) {
				setBlockAt(x, y, Block::Empty);
				_bonusSpawned = false;
				_bonusUnspawnTime = std::chrono::high_resolution_clock::now();
				_bonusPosition = iVector2(-1, -1);
				_nextBonusTime = (Board::_MaxBonusTime - Board::_MinBonusTime) * (static_cast<double>(std::rand()) / static_cast<double>(RAND_MAX)) + Board::_MinBonusTime;
				return ;
			}
		}
	}
}

void Board::reset() {
	_gameArea = std::vector<Block>(_gameAreaSize.x * _gameAreaSize.y, Block::Empty);

	for (int y = 0; y < _gameAreaSize.y; ++y) {
		for (int x = 0; x < _gameAreaSize.x; ++x) {
			if (y == 0 || y == _gameAreaSize.y - 1 || x == 0 || x == _gameAreaSize.x - 1) {
				setBlockAt(x, y, Block::Wall);
				continue;
			}
			if (_level == 1 && y % 3 == 0 && x % 3 == 0) {
				setBlockAt(x, y, Block::Wall);
				continue;
			}
			if (_level == 2 && y % 3 == 0 && x > 2 && x < _gameAreaSize.x - 3) {
				setBlockAt(x, y, Block::Wall);
				continue;
			}
			_emptyCells.push_back(iVector2(x, y));
		}
	}

	_bonusSpawned = false;
	_bonusUnspawnTime = std::chrono::high_resolution_clock::now();
	_bonusSpawnTime = std::chrono::high_resolution_clock::now();
	_nextBonusTime = 0.0;
	_bonusLifeTime = 0.0;
	_bonusPosition = iVector2(-1, -1);
}

const std::vector<Block>& Board::getGameArea() const {
	return _gameArea;
}

const iVector2& Board::getGameAreaSize() const {
	return _gameAreaSize;
}

iVector2 Board::getFoodPosition() const {
	return _foodPosition;
}

iVector2 Board::getBonusPosition() const {
	return _bonusPosition;
}

bool Board::isFilledWithSnakes() const {
	for (size_t i = 0; i < _gameArea.size(); i++) {
		if (_gameArea[i] == Block::Food || _gameArea[i] == Block::Bonus || _gameArea[i] == Block::Empty) {
			return false;
		}
	}
	return true;
}

Block Board::getBlockAt(int x, int y) {
	return _gameArea[y * _gameAreaSize.x + x];
}

Block Board::getBlockAt(iVector2 v) {
	return _gameArea[v.y * _gameAreaSize.x + v.x];
}

int Board::getLevel() const {
	return _level;
}


void Board::setBlockAt(int x, int y, Block block) {
	_gameArea[y * _gameAreaSize.x + x] = block;
}

void Board::setBlockAt(iVector2 v, Block block) {
	_gameArea[v.y * _gameAreaSize.x + v.x] = block;
}

void Board::setFoodAtLocation(iVector2 position) {
	setBlockAt(position, Food);
}

void Board::setBonusAtLocation(iVector2 position) {
	setBlockAt(position, Bonus);
}


const int Board::DefaultHeight	= 10;
const int Board::DefaultWidth	= 10;
const int Board::MinWidth		= 10;
const int Board::MinHeight		= 10;
const int Board::MaxWidth		= 38;
const int Board::MaxHeight		= 18;
const double Board::_MinBonusTime = 5.0;
const double Board::_MaxBonusTime = 15.0;

