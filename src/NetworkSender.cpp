#include "NetworkSender.hpp"
#include "Game.hpp"


NetworkSender::NetworkSender(Game& game): _game(game) {
	_board = _game.getBoard();
}

NetworkSender::~NetworkSender() {}
NetworkSender::NetworkSender(const NetworkSender& other): _game(other._game) {
	*this = other;
}

NetworkSender& NetworkSender::operator=(const NetworkSender&) {
	return *this;
}


void NetworkSender::sendReadySignal(INetwork* sender) {
	NetworkData networkData = NetworkData::Ready;
	char data[4];
	std::memcpy(data, &networkData, sizeof(NetworkData));
	sender->sendData(data, sizeof(data));
}

void NetworkSender::sendRestartSignal(INetwork* sender) {
	NetworkData networkData = NetworkData::Restart;
	char data[4];
	std::memcpy(data, &networkData, sizeof(NetworkData));
	sender->sendData(data, sizeof(data));
}

void NetworkSender::sendInitBoard(INetwork* sender) {
	NetworkData networkData = NetworkData::InitBoard;
	iVector2 boardSize = _board->getGameAreaSize();
	int level = _board->getLevel();
	char data[16];
	std::memcpy(data, &networkData, sizeof(NetworkData));
	std::memcpy(data + 4, &boardSize, sizeof(iVector2));
	std::memcpy(data + 12, &level, sizeof(int));
	sender->sendData(data, sizeof(data));
}

void NetworkSender::sendGameState(INetwork* sender, std::shared_ptr<Snake> snake1, std::shared_ptr<Snake> snake2, Sounds soundToPlay) {
	
	NetworkData networkData = NetworkData::GameState;
	NetworkGameData gameData;

	std::vector<iVector2> bodyPartsP1 = snake1->getBodyParts();
	std::vector<iVector2> bodyPartsP2 = snake2->getBodyParts();

	gameData.foodPosition = _board->getFoodPosition();
	gameData.bonusPosition = _board->getBonusPosition();
	gameData.snake1BodySize = bodyPartsP1.size();
	gameData.snake2BodySize = bodyPartsP2.size();
	gameData.snake1Score = snake1->getScore();
	gameData.snake2Score = snake2->getScore();
	gameData.snake1Direction = snake1->getDirection();
	gameData.snake2Direction = snake2->getDirection();
	gameData.snake1Alive = snake1->isAlive();
	gameData.snake2Alive = snake2->isAlive();
	gameData.winState = _game.getWinState();
	gameData.soundToPlay = soundToPlay;

	std::string data(4 + sizeof(NetworkGameData) + sizeof(iVector2) * (gameData.snake1BodySize + gameData.snake2BodySize), '0');

	std::memcpy(&data[0], &networkData, sizeof(NetworkData));
	std::memcpy(&data[4], &gameData, sizeof(NetworkGameData));
	
	std::memcpy(&data[4 + sizeof(NetworkGameData)], &bodyPartsP1[0], sizeof(iVector2) * gameData.snake1BodySize);
	std::memcpy(&data[4 + sizeof(NetworkGameData) + (sizeof(iVector2) * gameData.snake1BodySize)], &bodyPartsP2[0], sizeof(iVector2) * gameData.snake2BodySize);

	char* dataStr = data.data();
	sender->sendData(dataStr, data.size());
}

void NetworkSender::sendNextDirection(INetwork* sender, std::shared_ptr<Snake> snake) {
	NetworkData networkData = NetworkData::NextDirection;
	Direction direction = snake->getNextDirection();

	char data[8];
	std::memcpy(data, &networkData, sizeof(NetworkData));
	std::memcpy(data + 4, &direction, sizeof(Direction));
	sender->sendData(data, sizeof(data));
}




void NetworkSender::readReadySignal(std::shared_ptr<Snake> snake) {
	snake->setReady(true);
}

void NetworkSender::readRestartSignal() {
	_game.restart();
}

void NetworkSender::readGameState(char* data, std::shared_ptr<Snake> snake1, std::shared_ptr<Snake> snake2) {

	NetworkGameData gameData = *reinterpret_cast<NetworkGameData*>(data + 4);
	
	std::vector<iVector2> bodyPartsP1;
	std::vector<iVector2> bodyPartsP2;

	bodyPartsP1.resize(gameData.snake1BodySize);
	bodyPartsP2.resize(gameData.snake2BodySize);

	std::memcpy(&bodyPartsP1[0], data + 4 + sizeof(NetworkGameData), sizeof(iVector2) * gameData.snake1BodySize);
	std::memcpy(&bodyPartsP2[0], data + 4 + sizeof(NetworkGameData) + sizeof(iVector2) * gameData.snake1BodySize, sizeof(iVector2) * gameData.snake2BodySize);

	snake1->setBodyParts(bodyPartsP1);
	snake2->setBodyParts(bodyPartsP2);

	snake1->setScore(gameData.snake1Score);
	snake2->setScore(gameData.snake2Score);

	snake1->setDirection(gameData.snake1Direction);
	snake2->setDirection(gameData.snake2Direction);
	
	snake1->setAlive(gameData.snake1Alive);
	snake2->setAlive(gameData.snake2Alive);

	_board->setFoodAtLocation(gameData.foodPosition);
	if (gameData.bonusPosition.x > -1) {
		_board->unspawnBonus();
		_board->setBonusAtLocation(gameData.bonusPosition);
	} else {
		_board->unspawnBonus();
	}
	_game.setWinState(gameData.winState);
	if (gameData.soundToPlay >= 0) {
		_game.playSound(gameData.soundToPlay);
	}
}

void NetworkSender::readInitBoard(char* data, std::shared_ptr<Board>& board, std::shared_ptr<Snake>& snake1, std::shared_ptr<Snake>& snake2) {
	iVector2 boardSize = *reinterpret_cast<iVector2*>(data + 4);
	int level = *reinterpret_cast<int*>(data + 12);
	
	snake1 = std::make_shared<Snake>(boardSize, Game::DefaultTickDuration, true);
	snake2 = std::make_shared<Snake>(boardSize, Game::DefaultTickDuration, true);
	board = std::make_shared<Board>(boardSize, level);

	_board = board;

	if (!_game.getCurrentRenderer()) {
		_game.loadLibrary();
	}
	_game.resize(boardSize);
}

void  NetworkSender::readNextDirection(char* data, std::shared_ptr<Snake> snake) {
	Direction direction = *reinterpret_cast<Direction*>(data + 4);
	snake->setNextDirection(direction);
}


void NetworkSender::setBoard(std::shared_ptr<Board> board) {
	_board = board;
}
