#include "LibraryLoader_Unix.hpp"

LibraryLoader_Unix::LibraryLoader_Unix() {}
LibraryLoader_Unix::~LibraryLoader_Unix() {}


bool LibraryLoader_Unix::load(std::string fileName) {
	_dlHandle = dlopen(fileName.c_str(), RTLD_LAZY | RTLD_LOCAL);

	if (!_dlHandle) {		
		throw LibraryLoaderException("Could not load the library: " + fileName + " (" + dlerror() + ")");
	}
	return true;
}

void LibraryLoader_Unix::unload() {
	dlclose(_dlHandle);
}

LibraryLoader_Unix& LibraryLoader_Unix::operator=(const LibraryLoader_Unix&) {
	return *this;
}
LibraryLoader_Unix::LibraryLoader_Unix(const LibraryLoader_Unix& other) {
	*this = other;
}