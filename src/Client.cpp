#include "Client.hpp"

#include <bitset>

Client::Client() {	
	if ((_sock = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		std::cerr << "Socket creation failed" << std::endl;
		exit(EXIT_FAILURE);
	}
}

Client::Client(const Client& other) {
	*this = other;
}

Client& Client::operator=(const Client& other) {
	port = other.port;
	address = other.address;
	return *this;
}

Client::~Client() {
	if (_sock) {
		close(_sock);
	}
}

bool Client::openConnection() {

	_sin.sin_addr.s_addr = inet_addr(address.c_str());
	_sin.sin_family = AF_INET;
	_sin.sin_port = htons(port);

	if (connect(_sock, (sockaddr*)& _sin, sizeof(_sin)) != SOCKET_ERROR) {
		Logger::Log << "Connected at " << inet_ntoa(_sin.sin_addr) << ":" << htons(_sin.sin_port) << "\n";
		return true;
	} else {
		std::cerr << "Connection error" << std::endl;
		return false;
	}
	return true;
}

void Client::sendData(const char* data, int length) {
	send(_sock, data, length, 0);

}

std::string Client::receiveData() {
	char buffer[4096] = {0};
	std::string data = "";

	while (int size = recv(_sock, buffer, sizeof(buffer), MSG_DONTWAIT)) {
		if (size == -1) {
			break;
		}
		data += std::string(&buffer[0], &buffer[size]);
		memset(buffer, 0, sizeof(buffer));
	}
	return data;
}
