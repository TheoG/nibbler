#include "Common.hpp"
#include "Board.hpp"
#include "Snake.hpp"
#include "Game.hpp"

#include "Server.hpp"
#include "Client.hpp"

#include <args.hxx>
#include <random>

int main(int argc, char *argv[]) {
	bool checkForLeaks = false;

	{

	args::ArgumentParser parser("Options", "Thanks for playing.");
	args::HelpFlag help(parser, "help", "Display this help menu", { 'h', "help" });
	args::ValueFlag<int> widthArgs(parser, "Width", "Game area's width", { "width" }, Board::DefaultWidth);
	args::ValueFlag<int> heightArgs(parser, "Height", "Game area's height", { "height" }, Board::DefaultHeight);
	args::ValueFlag<int> difficultyArgs(parser, "Difficulty", "Choose the game difficulty", { 'd', "difficulty" });
	args::ValueFlag<int> levelArgs(parser, "Level", "Choose the game level", { 'l', "level" });
	args::ValueFlag<int> serverPortArgs(parser, "Port", "Port of the server", { 'p', "port" });
	args::ValueFlag<std::string> serverAddressArgs(parser, "Address", "Address of the server", { 'a', "address" });

	args::ValueFlag<int> guiLibIndexArgs(parser, "GUI index", "Index of the GUI lib to load first", { "gui", 'g' });

	args::Flag clientArgs(parser, "Client", "Run the game in network multiplayer, client side", { 'c', "client" });
	args::Flag serverArgs(parser, "Server", "Run the game in network multiplayer, server side", { 's', "server" });
	args::Flag localMultiplayerArgs(parser, "Multiplayer", "Run the game in local multiplayer", { 'm', "multiplayer" });
	args::Flag leaksArgs(parser, "Leaks", "Infinite loop to check the leaks", { "leaks" });

	try {
		parser.ParseCLI(argc, argv);
	}
	catch (const args::Completion& e) {
		std::cerr << e.what() << std::endl;
		exit(EXIT_SUCCESS);
	}
	catch (const args::Help& e) {
		std::cout << parser << std::endl;
		exit(EXIT_SUCCESS);
	}
	catch (const args::ParseError& e) {
		std::cerr << e.what() << std::endl;
		std::cerr << parser;
		exit(EXIT_FAILURE);
	}

	std::srand(time(NULL));

	
	int width = std::clamp(args::get(widthArgs), Board::MinWidth, Board::MaxWidth);
	int height = std::clamp(args::get(heightArgs), Board::MinHeight, Board::MaxHeight);
	
	int guiLibIndex = 1;
	int difficulty = 1;
	int level = 1;

	if (difficultyArgs)
		difficulty = std::clamp(args::get(difficultyArgs), 1, 3);

	if (guiLibIndexArgs) {
		guiLibIndex = std::clamp(args::get(guiLibIndexArgs), 1, NB_DLL);
	}

	if (levelArgs) {
		level = std::clamp(args::get(levelArgs), 1, 3);
	}

	if ((localMultiplayerArgs && clientArgs) || (localMultiplayerArgs && serverArgs)) {
		std::cerr << "Cannot be both local multiplayer and network multiplayer" << std::endl;
		std::exit(EXIT_FAILURE);
	}

	if (leaksArgs) {
		checkForLeaks = true;
	}

#ifdef WIN32
	if (clientArgs || serverArgs) {
		WSADATA WSAData;
		int error = WSAStartup(MAKEWORD(2, 2), &WSAData);
		if (error) {
			std::cerr << "Error with sockets creation (" << error << ")" << std::endl;
			exit(EXIT_FAILURE);
		}
	}
#endif

	GameMode gameMode = GameMode::Solo;
	if (clientArgs) {
		gameMode = GameMode::NetworkMultiplayerClient;
	} else if (serverArgs) {
		gameMode = GameMode::NetworkMultiplayerHost;
	} else if (localMultiplayerArgs) {
		gameMode = GameMode::LocalMultiplayer;
	}

	std::unique_ptr<Game> game = std::make_unique<Game>(iVector2(width, height), gameMode, difficulty, level - 1);
	if (clientArgs) {
		if (serverPortArgs) {
			game->setServerPort(args::get(serverPortArgs));
		}
		if (serverAddressArgs) {
			game->setServerAddress(args::get(serverAddressArgs));
		}
		game->launchClient();
	} else if (serverArgs) {
		if (serverPortArgs) {
			game->setServerPort(args::get(serverPortArgs));
		}
		game->launchServer();
	}
	game->loadLibrary(guiLibIndex - 1);
	game->loop();


#ifdef WIN32
	if (clientArgs || serverArgs) {
		WSACleanup();
	}
#endif
	}

	if (checkForLeaks) {
		while (true);
	}

	return 0;
}
