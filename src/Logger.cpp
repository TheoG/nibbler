#include "Logger.hpp"

Logger::Logger() {
	if (!_file.is_open()) {
		_file.open("log.txt", std::ios::trunc);
		if (!_file.is_open()) {
			std::cerr << "Error: can't open log.txt" << std::endl;
			return ;
		}
	}
}

Logger::~Logger() {
	if (_file.is_open())
		_file.close();
}

void Logger::setFileLog(bool fileLog) {
	Logger::_fileLog = fileLog;
}

Logger& Logger::operator=(const Logger&) {
	return *this;
}

Logger::Logger(const Logger& other) {
	*this = other;
}

bool Logger::_fileLog = false;
std::ofstream Logger::_file;
std::ostream& Logger::os = std::cout;
Logger Logger::Log;
