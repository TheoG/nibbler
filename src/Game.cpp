#include "Game.hpp"
#include <cmath>
#include <algorithm>
#include "exceptions/LibraryLoaderException.hpp"

Game::Game(iVector2 boardSize, GameMode gm, int difficulty, int level):
	_currentRenderer(nullptr),
	_gameMode(gm),
	_elapsedTime(0.0),
	_shouldRun(true),
	_started(false),
	_winner(WinState::NoOne),
	_difficulty(difficulty),
	_serverAddress("127.0.0.1"),
	_serverPort(4242),
	_client(nullptr),
	_server(nullptr),
	_connectionTimeout(2.0),
	_connectionElaspedTime(0.0) {

	_renderers.resize(NB_DLL);
	_loaders.resize(NB_DLL + 1);

	if (_gameMode != GameMode::NetworkMultiplayerClient) {
		_board = std::make_shared<Board>(boardSize, level);
		_snake1 = std::make_shared<Snake>(boardSize, Game::DefaultTickDuration, _gameMode != GameMode::Solo);

		if (_gameMode != GameMode::Solo) {
			_snake2 = std::make_shared<Snake>(boardSize, Game::DefaultTickDuration,  _gameMode != GameMode::Solo);
		}
	} else {
		_connectionTime = std::chrono::steady_clock::now();
	}

	_loadLibraries(boardSize);
}

Game::Game(const Game& other) {
	*this = other;
}

Game& Game::operator=(const Game& other) {
	_currentRenderer = other._currentRenderer;
	_shouldRun = other._shouldRun;
	_elapsedTime = other._elapsedTime;
	_gameMode = other._gameMode;
	_difficulty = other._difficulty;
	_started = other._started;
	_client = other._client;
	_server = other._server;
	_renderers = other._renderers;
	_loaders = other._loaders;
	_currentLibraryIndex = other._currentLibraryIndex;
	_sender = other._sender;
	_snake1 = other._snake1;
	_snake2 = other._snake2;
	_board = other._board;
	return *this;
}

Game::~Game() {
	try {

		auto deleteSFMLRenderer = _loaders[0]->resolveFunction<void(IRenderer*)>("deleteSFMLRenderer");
		if (deleteSFMLRenderer) {
			deleteSFMLRenderer(_renderers[0]);
		}
	} catch (const LibraryLoaderException& e) {
		std::cerr << "SFML Exception: " << e.what() << std::endl;
	}

	try {
		auto deleteSDLRenderer = _loaders[1]->resolveFunction<void(IRenderer*)>("deleteSDLRenderer");
		if (deleteSDLRenderer) {
			deleteSDLRenderer(_renderers[1]);
		}
	} catch (const LibraryLoaderException& e) {
		std::cerr << "SDL Exception: " << e.what() << std::endl;
	}

	try {
		auto deleteSoundSystem = _loaders[_loaders.size() - 1]->resolveFunction<void(ISound*)>("deleteSound");
		if (deleteSoundSystem) {
			deleteSoundSystem(_soundSystem);
		}
	} catch (const LibraryLoaderException& e) {
		std::cerr << "SFML Exception: " << e.what() << std::endl;
	}


#ifndef WIN32
	try {
		auto deleteNCursesRenderer = _loaders[2]->resolveFunction<void(IRenderer*)>("deleteNcursesRenderer");
		if (deleteNCursesRenderer) {
			deleteNCursesRenderer(_renderers[2]);
		}
	} catch (const LibraryLoaderException& e) {
		std::cerr << "NCurses Exception: " << e.what() << std::endl;
	}
#endif

	for (auto loader : _loaders) {
		if (loader) {
			loader->unload();
			delete loader;
		}
	}
	if (_server) {
		delete _server;
	}
	if (_client) {
		delete _client;
	}
}

void Game::updateGameState() {


	bool updatedSnake1 = false;
	bool updatedSnake2 = false;
	bool foodHasBeenEaten = false;
	bool bonusHasBeenEaten = false;
	bool shouldPlaySound = false;
	Sounds sound;

	if (_snake1->isAlive() && _snake1->update(_elapsedTime)) {
		updatedSnake1 = true;
		if (!_snake1->moveForward(_board)) {
			_snake1->setAlive(false);
			shouldPlaySound = true;
			sound = Sounds::Death;
			_soundSystem->stop(Sounds::Music);
			if (_snake2) {
				_winner = WinState::Player2;
			} else {
				_winner = WinState::Lose;
			}
		}
		_snake1->setTickDuration((1 / ((std::pow(_elapsedTime, 2)) / 10000.0 + _difficulty * 2)));
	}

	if (_snake2 && _snake2->isAlive() && _snake2->update(_elapsedTime)) {
		updatedSnake2 = true;
		if (!_snake2->moveForward(_board)) {
			_snake2->setAlive(false);
			sound = Sounds::Death;
			shouldPlaySound = true;
			_soundSystem->stop(Sounds::Music);
			_winner = WinState::Player1;
		}
		_snake2->setTickDuration((1 / ((std::pow(_elapsedTime, 2)) / 10000.0 + _difficulty * 2)));
	}

	if (updatedSnake1) {
		Block block = _board->getBlockAt(_snake1->getHeadPosition());
		if (block == Block::Food) {
			_snake1->eat();
			foodHasBeenEaten = true;
		} else if (block == Block::Bonus) {
			_snake1->bonus();
			bonusHasBeenEaten = true;
		}
	}

	if (updatedSnake2) {
		Block block = _board->getBlockAt(_snake2->getHeadPosition());
		if (block == Block::Food) {
			_snake2->eat();
			foodHasBeenEaten = true;
		} else if (block == Block::Bonus) {
			_snake2->bonus();
			bonusHasBeenEaten = true;
		}
	}

	if (foodHasBeenEaten) {
		sound = Sounds::Eat;
		shouldPlaySound = true;
		auto pos = _board->generateFoodAtRandomLocation();
	}
	if (bonusHasBeenEaten) {
		sound = Sounds::PowerUp;
		shouldPlaySound = true;
		_board->unspawnBonus();
	}

	if (_snake2 && (updatedSnake1 || updatedSnake2) && (_snake1->getHeadPosition() == _snake2->getHeadPosition() || (!_snake1->isAlive() && !_snake2->isAlive()))) {
		_snake1->setAlive(false);
		_snake2->setAlive(false);
		sound = Sounds::Death;
		shouldPlaySound = true;
		_soundSystem->stop(Sounds::Music);
		_winner = WinState::Draw;
	}

	if (shouldPlaySound) {
		playSound(sound);
	}

	bool updateBonus = false;
	if (_gameMode != GameMode::NetworkMultiplayerClient && _snake1->isAlive() && (!_snake2 || _snake2->isAlive())) {
		updateBonus = _board->updateBonus();
	}

	if ((updatedSnake1 || updatedSnake2 || updateBonus) && _gameMode == GameMode::NetworkMultiplayerHost) {
		_sender->sendGameState(_server, _snake1, _snake2, sound);
	}
}

void Game::getEvents() {
	if (!_currentRenderer) {
		return ;
	}
	auto events = _currentRenderer->getEvents();

	for (auto event: events) {
		if (event == Input::Nibbler_Escape) {
			_shouldRun = false;
		}

		auto tryChangeDirection = [this](Input event, Input eventToCheck, std::shared_ptr<Snake> snake) {
			if (event == eventToCheck) {
				auto direction = snake->getDirection();
				auto opposedDirection = _OpposedDirection.find(direction)->second;
				auto nextDirection = _InputToDirection.find(eventToCheck)->second;

				if (nextDirection != opposedDirection && nextDirection != direction) {
					snake->setNextDirection(nextDirection);
				}
				if (_gameMode == GameMode::NetworkMultiplayerClient) {
					_sender->sendNextDirection(_client, _snake2);
				}
			}
		};

		// Only if solo, local multiplayer or (network multilayer AND host)
		if (_gameMode != GameMode::NetworkMultiplayerClient) {
			tryChangeDirection(event, Input::Nibbler_Down,	_snake1);
			tryChangeDirection(event, Input::Nibbler_Up,	_snake1);
			tryChangeDirection(event, Input::Nibbler_Left,	_snake1);
			tryChangeDirection(event, Input::Nibbler_Right,	_snake1);
		}

		// Only if local multiplayer or (network multilayer AND client)
		if (_snake2 && _gameMode == GameMode::LocalMultiplayer) {
			tryChangeDirection(event, Input::Nibbler_S,	_snake2);
			tryChangeDirection(event, Input::Nibbler_W,	_snake2);
			tryChangeDirection(event, Input::Nibbler_A,	_snake2);
			tryChangeDirection(event, Input::Nibbler_D,	_snake2);
		}

		if (_snake2 && _gameMode == GameMode::NetworkMultiplayerClient) {
			tryChangeDirection(event, Input::Nibbler_Down,	_snake2);
			tryChangeDirection(event, Input::Nibbler_Up,	_snake2);
			tryChangeDirection(event, Input::Nibbler_Left,	_snake2);
			tryChangeDirection(event, Input::Nibbler_Right,	_snake2);
		}



		// All
		if (event == Input::Nibbler_1 && _currentRenderer != _renderers[0]) {
			loadLibrary(0);
		} else if (event == Input::Nibbler_2 && _currentRenderer != _renderers[1]) {
			loadLibrary(1);
		} else if (event == Input::Nibbler_3 && _currentRenderer != _renderers[2]) {
			loadLibrary(2);
		}

		// Only if solo, local multiplayer or (network multiplayer AND host)
		if (_gameMode != GameMode::NetworkMultiplayerClient && event == Input::Nibbler_R) {
			restart();
			if (_gameMode == GameMode::NetworkMultiplayerHost) {
				_sender->sendRestartSignal(_server);
			}
		}

		if (event == Input::Nibbler_Space) {
			if (_gameMode == GameMode::Solo) {
				_snake1->setReady(true);
			} else if (_gameMode == GameMode::LocalMultiplayer) {
				_snake1->setReady(true);
				_snake2->setReady(true);
			} else if (_gameMode == GameMode::NetworkMultiplayerHost) {
				_snake1->setReady(true);
				_sender->sendReadySignal(_server);
			} else if (_gameMode == GameMode::NetworkMultiplayerClient) {
				_snake2->setReady(true);
				_sender->sendReadySignal(_client);
			}
		}
	}
}

void Game::updateUI() {
	if (!_currentRenderer) {
		return ;
	}

	if (!_snake2) {
		_board->updateSnakePosition(_snake1->getBodyParts());
	} else {
		_board->updateSnakesPositions(_snake1->getBodyParts(), _snake2->getBodyParts());
	}

	GameData data = {
		_board->getGameArea(),
		(_snake1 && !_snake1->isAlive() && (!_snake2 || (_snake2 && !_snake2->isAlive()))) ? _endTime : _endTime = _elapsedTime,
		_snake1->getScore(),
		_snake2 ? _snake2->getScore() : -1,
		_snake1->getDirection(),
		_snake2 ? _snake2->getDirection() : _snake1->getDirection(),
		_snake1->isReady(),
		_snake2 ? _snake2->isReady() : false,
		_winner
	};

	_currentRenderer->updateUI(data);
}

void Game::loadLibrary() {
	loadLibrary(_currentLibraryIndex);
}

void Game::loadLibrary(int index) {
	_currentLibraryIndex = std::clamp(index, 0, NB_DLL);

	if (_currentRenderer) {
		_currentRenderer->deinit();
		_currentRenderer = nullptr;
	}

	_currentRenderer = _renderers[index];
	if (_currentRenderer) {
		_currentRenderer->init();
		if (index == NCURSES)
			Logger::setFileLog(true);
		else
			Logger::setFileLog(false);
	}
}

void Game::loop() {
	while (_shouldRun) {
		if (!_board) {
			if (_gameMode == GameMode::NetworkMultiplayerClient) {
				_connectionElaspedTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _connectionTime).count() / 1000.0;
				if (_connectionElaspedTime >= _connectionTimeout) {
					_shouldRun = false;
					std::cout << "Connection timeout after " << _connectionElaspedTime << "s..." << std::endl;
				}
			}
			continue ;
		}
		loopMutex.lock();
		getEvents();

		if (_started) {
			if (_winner == WinState::NoOne) {
				_elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _startTime).count() / 1000.0;
				if (_gameMode != GameMode::NetworkMultiplayerClient) {
					updateGameState();
				}
			}
		} else {
			if (_snake1) {
				if (_snake2 ? _snake1->isReady() && _snake2->isReady() : _snake1->isReady()) {
					start();
				}
			}
		}
		updateUI();
		if (_board->isFilledWithSnakes()) {
			_winner = WinState::Win;
		}
		loopMutex.unlock();
	}

	_currentRenderer->deinit();

}

void Game::start() {
	playSound(Sounds::Music);
	_started = true;
	_elapsedTime = 0.0;
	_startTime = std::chrono::steady_clock::now();
	_winner = WinState::NoOne;
	if (_gameMode != GameMode::NetworkMultiplayerClient) {
		iVector2 position = _board->generateFoodAtRandomLocation();
	}
}

void Game::restart() {
	_started = false;
	_elapsedTime = 0.0;
	_startTime = std::chrono::steady_clock::now();
	_winner = WinState::NoOne;
	_board->reset();
	_snake1->reset(_board->getGameAreaSize(), 1);
	if (_snake2)
		_snake2->reset(_board->getGameAreaSize(), 2);
}

void Game::playSound(Sounds sound) {
	_soundSystem->play(sound);
}

void Game::resize(iVector2 boardSize) {
	for (auto renderer : _renderers) {
		if (renderer) {
			renderer->resize(boardSize);
		}
	}
}

std::shared_ptr<Board> Game::getBoard() {
	return _board;
}

IRenderer* Game::getCurrentRenderer() {
	return _currentRenderer;
}

const WinState& Game::getWinState() const {
	return _winner;
}

void Game::setWinState(WinState winState) {
	if (_winner != winState) {
		if (winState != WinState::NoOne) {
			_soundSystem->stop(Sounds::Music);
		}
	}
	_winner = winState;
}

void Game::setServerAddress(const std::string& address) {
	_serverAddress = address;
}

void Game::setServerPort(int port) {
	_serverPort = port;
}

void Game::launchServer() {
	try {
		_sender = std::make_shared<NetworkSender>(*this);
		std::thread([this]() {
			Logger::Log << "Launching server...\n";
			_server = new Server();
			_server->port = _serverPort;
			if (!_server->openConnection()) {
				std::cerr << "The server cannot open the connection." << std::endl;
				_shouldRun = false;
				return ;
			} else {
				_sender->sendInitBoard(_server);
			}
			while (_shouldRun) {
				std::string dataString = _server->receiveData();
				char* data = nullptr;
				if (dataString.length()) {
					data = dataString.data();
				}
				if (data) {
					loopMutex.lock();
					int value = *reinterpret_cast<int*>(data);
					if (value == NetworkData::Ready) {
						_sender->readReadySignal(_snake2);
					} else if (value == NetworkData::NextDirection) {
						_sender->readNextDirection(data, _snake2);
					}
					loopMutex.unlock();
				}
			}
		}).detach();
	} catch (const std::exception& e) {
		std::cerr << "Exception while running the thread: " << e.what() << std::endl;
	}
}

void Game::launchClient() {
	try {
		_sender = std::make_shared<NetworkSender>(*this);
		std::thread([this]() {
			Logger::Log << "Launching client...";
			_client = new Client();
			_client->port = _serverPort;
			_client->address = _serverAddress;
			_connectionTime = std::chrono::steady_clock::now();
			if (!_client->openConnection()) {
				std::cerr << "The client cannot connect to the server." << std::endl;
				_shouldRun = false;
				return;
			}
			while (_shouldRun) {
				std::string dataString = _client->receiveData();
				char* data = nullptr;
				if (dataString.length()) {
					data = dataString.data();
				}

				if (data) {
					loopMutex.lock();
					int value = *reinterpret_cast<int*>(data);
					if (value == NetworkData::Ready) {
						_sender->readReadySignal(_snake1);
					} else if (value == NetworkData::Restart) {
						_sender->readRestartSignal();
					} else if (value == NetworkData::InitBoard) {
						_sender->readInitBoard(data, _board, _snake1, _snake2);
					} else if (value == NetworkData::GameState) {
						_sender->readGameState(data, _snake1, _snake2);
					}
					loopMutex.unlock();
				}
			}
		}).detach();
	} catch (const std::exception& e) {
		std::cerr << "Exception while running the thread: " << e.what() << std::endl;
	}
}

void Game::_loadLibraries(iVector2 boardSize) {
#ifdef WIN32
	_loaders[0] = new LibraryLoader_Win(); // SFML
	_loaders[1] = new LibraryLoader_Win(); // SDL
	_loaders[_loaders.size() - 1] = new LibraryLoader_Win(); // NCurses

	_loaders[0]->load("H:\\Projects\\Nibbler\\GUIs\\sfml\\build\\sfml-renderer-d.dll");
	_loaders[_loaders.size() - 1]->load("H:\\Projects\\Nibbler\\SFX\\sfml\\build\\sfml-sound-d.dll");
#else

	_loaders[0] = new LibraryLoader_Unix(); // SFML
	_loaders[1] = new LibraryLoader_Unix(); // SDL
	_loaders[2] = new LibraryLoader_Unix(); // NCurses
	_loaders[_loaders.size() - 1] = new LibraryLoader_Unix(); // Sounds

	try {
		_loaders[0]->load("./GUIs/sfml/build/libsfml-renderer.dylib");
		_loaders[1]->load("./GUIs/sdl/build/libsdl-renderer.dylib");
		_loaders[2]->load("./GUIs/ncurses/build/libncurses-renderer.dylib");
		_loaders[_loaders.size() - 1]->load("./SFX/sfml/build/libsfml-sound.dylib");
	} catch (const LibraryLoaderException& e) {
		std::cerr << e.what() << std::endl;
		std::exit(EXIT_FAILURE);
	}

#endif

	try {
		
		auto createSFMLRenderer = _loaders[0]->resolveFunction<IRenderer * (iVector2 v)>("createSFMLRenderer");
		if (createSFMLRenderer) {
			_renderers[0] = createSFMLRenderer(boardSize);
		}

		auto createSDLRenderer = _loaders[1]->resolveFunction<IRenderer * (iVector2 v)>("createSDLRenderer");
		if (createSDLRenderer) {
			_renderers[1] = createSDLRenderer(boardSize);
		}

		auto createSoundSystem = _loaders[_loaders.size() - 1]->resolveFunction<ISound * (void)>("createSound");
		if (createSoundSystem) {
			_soundSystem = createSoundSystem();
			_soundSystem->init();
		}


	#ifndef WIN32
		auto createNcursesRenderer = _loaders[2]->resolveFunction<IRenderer * (iVector2 v)>("createNcursesRenderer");
		if (createNcursesRenderer) {
			_renderers[2] = createNcursesRenderer(boardSize);
		}
	#endif
	} catch (const LibraryLoaderException& e) {
		std::cerr << e.what() << std::endl;
		std::exit(EXIT_FAILURE);
	}


}

const std::map<Input, Direction> Game::_InputToDirection = {
	{ Input::Nibbler_Left,	Direction::West 	},
	{ Input::Nibbler_A,		Direction::West 	},
	{ Input::Nibbler_Up,	Direction::North	},
	{ Input::Nibbler_W,		Direction::North	},
	{ Input::Nibbler_Down,	Direction::South	},
	{ Input::Nibbler_Right,	Direction::East		},
	{ Input::Nibbler_D,		Direction::East		},
	{ Input::Nibbler_S,		Direction::South	},
};

const std::map<Direction, Direction> Game::_OpposedDirection = {
	{ Direction::North,	Direction::South },
	{ Direction::South,	Direction::North },
	{ Direction::East,	Direction::West	 },
	{ Direction::West,	Direction::East	 },
};

const float Game::DefaultTickDuration = 0.5f;
