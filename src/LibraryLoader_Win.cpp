#include "LibraryLoader_Win.hpp"

LibraryLoader_Win::LibraryLoader_Win() {}
LibraryLoader_Win::~LibraryLoader_Win() {}


bool LibraryLoader_Win::load(std::string fileName) {
	_procIdll = LoadLibrary(fileName.c_str());

	if (!_procIdll) {
		throw LibraryLoaderException("Could not load the library: " + fileName + " " + std::to_string(GetLastError()));
	}
	return true;
}

LibraryLoader_Win& LibraryLoader_Win::operator=(const LibraryLoader_Win&) {
	return *this;
}
LibraryLoader_Win::LibraryLoader_Win(const LibraryLoader_Win& other) {
	*this = other;
}