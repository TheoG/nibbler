#include "Server.hpp"

Server::Server(): _csock(0) {
	
	if ((_sock = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		std::cerr << "Socket creation failed" << std::endl;
		exit(EXIT_FAILURE);
	}
}

Server::~Server() {
	if (_sock != 0) {
		close(_sock);
	}
	if (_csock != 0) {
		close(_csock);
	}
}

Server::Server(const Server& other) {
	*this = other;
}

Server& Server::operator=(const Server&) {
	return *this;
}

bool Server::openConnection() {
	socklen_t recsize = sizeof(_sin);

	_sin.sin_addr.s_addr = htonl(INADDR_ANY);
	_sin.sin_family = AF_INET;
	_sin.sin_port = htons(port);

	if (bind(_sock, (sockaddr*)&_sin, recsize) == SOCKET_ERROR) {
		#ifdef WIN32
		std::cerr << "Bind error " << WSAGetLastError() << std::endl;
		#else
		std::cerr << "Bind error" << std::endl;
		#endif
		return false;
	}

	if (listen(_sock, 5) == SOCKET_ERROR) {
		std::cerr << "Listening error" << std::endl;
		return false;
	}

	socklen_t crecsize = sizeof(_csin);

	if ((_csock = accept(_sock, (sockaddr*)&_csin, (socklen_t*)&crecsize)) < 0) {
		std::cerr << "Accept socket error" << std::endl;
		return false;
	}
	Logger::Log << "Client connection (" << _csock << ") at address " << inet_ntoa(_csin.sin_addr) << ":" <<  htons(_csin.sin_port) << "\n";
	return true;
}

void Server::sendData(const char* data, int length) {
	send(_csock, data, length, 0);
}

std::string Server::receiveData() {
	char buffer[4096] = {0};
	std::string data = "";

	while (int size = recv(_csock, buffer, sizeof(buffer), MSG_DONTWAIT)) {
		if (size == -1) {
			break;
		}
		data += std::string(&buffer[0], &buffer[size]);
		memset(buffer, 0, sizeof(buffer));
	}
	return data;
}
