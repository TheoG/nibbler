#pragma once

#include <Common.hpp>
#include "Logger.hpp"
#include "Types.hpp"

class Board {

public:

	Board(iVector2 gameAreaSize, int level);
	~Board();

	// Update snakes in the board
	void updateSnakePosition(const std::vector<iVector2>&);
	void updateSnakesPositions(const std::vector<iVector2>&, const std::vector<iVector2>&);

	// Generate food and bonuses
	iVector2 generateFoodAtRandomLocation();
	iVector2 generateBonusAtRandomLocation();
	bool updateBonus();
	void unspawnBonus();

	// Reset the board
	void reset();

	// Getters
	const std::vector<Block>& getGameArea() const;
	const iVector2& getGameAreaSize() const;
	iVector2 getFoodPosition() const;
	iVector2 getBonusPosition() const;
	bool isFilledWithSnakes() const;
	Block getBlockAt(int x, int y);
	Block getBlockAt(iVector2);
	int getLevel() const;

	// Setters
	void setBlockAt(int x, int y, Block block);
	void setBlockAt(iVector2, Block block);
	void setFoodAtLocation(iVector2 position);
	void setBonusAtLocation(iVector2 position);


	// Constants
	static const int DefaultWidth;
	static const int DefaultHeight;
	static const int MaxWidth;
	static const int MaxHeight;
	static const int MinWidth;
	static const int MinHeight;


private:
	Board();
	Board& operator=(const Board&);
	Board(const Board&);

	iVector2 _gameAreaSize;
	std::vector<Block> _gameArea;
	std::vector<iVector2> _emptyCells;

	int _level;

	std::chrono::high_resolution_clock::time_point _bonusSpawnTime;
	std::chrono::high_resolution_clock::time_point _bonusUnspawnTime;
	double _nextBonusTime;
	double _bonusLifeTime;
	bool _bonusSpawned;

	iVector2 _foodPosition;
	iVector2 _bonusPosition;

	static const double _MinBonusTime;
	static const double _MaxBonusTime;
};
