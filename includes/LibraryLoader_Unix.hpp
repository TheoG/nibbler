#pragma once

#include "Common.hpp"
#include "exceptions/LibraryLoaderException.hpp"

class LibraryLoader_Unix {

public:

	LibraryLoader_Unix();
	virtual ~LibraryLoader_Unix();

	bool load(std::string fileName);
	void unload();

	template <typename T>
	std::function<T> resolveFunction(const std::string& funcName) {
		
		void* func = dlsym(_dlHandle, funcName.c_str());
		
		if (!func) {
			throw LibraryLoaderException("Could not locate the function " + funcName + " (" + dlerror() + ")");
		}

		return reinterpret_cast<T*>(func);
	}

private:

	LibraryLoader_Unix& operator=(const LibraryLoader_Unix&);
	LibraryLoader_Unix(const LibraryLoader_Unix&);

	void* _dlHandle;
};