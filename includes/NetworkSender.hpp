#pragma once

class Game;
#include "Snake.hpp"
#include "INetwork.hpp"

class NetworkSender {

public:

	NetworkSender(Game& game);
	~NetworkSender();

	void sendReadySignal(INetwork* sender);
	void sendRestartSignal(INetwork* sender);
	void sendInitBoard(INetwork* sender);
	void sendGameState(INetwork* sender, std::shared_ptr<Snake> snake1, std::shared_ptr<Snake> snake2, Sounds soundToPlay);
	void sendNextDirection(INetwork* sender, std::shared_ptr<Snake> snake);

	void readReadySignal(std::shared_ptr<Snake> snake);
	void readRestartSignal();
	void readInitBoard(char* data, std::shared_ptr<Board>& board, std::shared_ptr<Snake>& snake1, std::shared_ptr<Snake>& snake2);
	void readGameState(char* data, std::shared_ptr<Snake> snake1, std::shared_ptr<Snake> snake2);
	void readNextDirection(char* data, std::shared_ptr<Snake> snake);

	void setBoard(std::shared_ptr<Board> board);

private:

	NetworkSender(const NetworkSender&);
	NetworkSender& operator=(const NetworkSender&);

	Game& _game;
	std::shared_ptr<Board> _board;

};