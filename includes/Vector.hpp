#pragma once

typedef struct iVector2 {

	iVector2();
	~iVector2();
	iVector2(int x, int y);
	iVector2(const iVector2&);
	iVector2& operator+=(const iVector2&);
	iVector2& operator=(const iVector2&);
	bool operator!=(const iVector2&) const;
	bool operator==(const iVector2&) const;

	int x;
	int y;

} iVector2;