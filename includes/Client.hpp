#pragma once

#include <Common.hpp>

#ifdef WIN32
 # include <WinSock2.h>
 # include <WS2tcpip.h>
 # include <io.h>
#else
 # include <sys/socket.h>
 # include <sys/types.h>
 # include <netinet/in.h>
 # include <arpa/inet.h>
 # include <unistd.h>
 typedef int SOCKET;
#endif

#include "Logger.hpp"
#include "INetwork.hpp"

class Client: public INetwork {

public:

	Client();
	virtual ~Client();

	int port;
	std::string address;

	bool openConnection();
	void sendData(const char* data, int length);
	std::string receiveData();

private:

	Client(const Client&);
	Client& operator=(const Client&);

	SOCKET _sock;
	sockaddr_in _sin;


};
