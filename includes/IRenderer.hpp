#pragma once

#include "Common.hpp"
#include "Types.hpp"

class IRenderer {

public:

	virtual void init() = 0;
	virtual void deinit() = 0;

	virtual std::vector<Input> getEvents() = 0;
	virtual void updateUI(const GameData&) = 0;
	virtual void resize(iVector2 newSize) = 0;
};
