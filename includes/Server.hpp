#pragma once

#include <Common.hpp>

#ifdef WIN32

#else
 # include <sys/types.h>
 # include <sys/socket.h>
 # include <arpa/inet.h>
 # include <netinet/in.h>
 # include <unistd.h>
 typedef int SOCKET;
#endif

#include "Logger.hpp"
#include "INetwork.hpp"

class Server: public INetwork {

public:

	Server();
	virtual ~Server();

	int port;

	bool openConnection();
	void sendData(const char* data, int length);
	std::string receiveData();
private:

	Server(const Server&);
	Server& operator=(const Server&);

	// Server attributes
	sockaddr_in _sin;
	SOCKET _sock;

	// Potential client attributes
	sockaddr_in _csin;
	SOCKET _csock;


};
