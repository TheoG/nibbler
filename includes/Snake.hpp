#pragma once

#include <Common.hpp>
#include <Board.hpp>
#include "Logger.hpp"
#include "Types.hpp"

class Snake {

public:

	Snake(iVector2 boardSize, float tickDuration, bool multiplayer = false);
	Snake(const Snake&);
	~Snake();

	Snake& operator=(const Snake&);

	bool moveForward(std::shared_ptr<Board> board);
	bool update(double elapsedTime);

	void eat();
	void bonus();
	void increaseScore(int value);

	void reset(iVector2 boardSize, int player);

	// Getters
	const std::vector<iVector2>& getBodyParts() const;
	const iVector2 getHeadPosition() const;
	Direction getNextDirection() const;
	Direction getDirection() const;
	Block getPlayerBlock() const;
	int getTickDuration() const;
	int getScore() const;
	int getIndex() const;
	int getSize() const;
	bool isAlive() const;
	bool isReady() const;

	// Setters
	void setBodyParts(std::vector<iVector2>& bodyParts);
	void setTickDuration(float tickDuration);
	void setNextDirection(Direction d);
	void setDirection(Direction d);
	void setAlive(bool alive);
	void setScore(int score);
	void setReady(bool ready);

private:

	Direction _direction;
	Direction _nextDirection;

	std::vector<iVector2> _bodyParts;
	Block _playerBlock;
	Bonuses _bonus;

	int _size;
	int _index;
	int _score;
	int _tickIndex;

	bool _hasEaten;
	bool _alive;
	bool _multiplayer;
	bool _ready;

	float _tickDuration;
	bool _speedUp;
	bool _speedDown;
	double _speedBonusTimeStamp;

	static int _nbPlayers;
};
