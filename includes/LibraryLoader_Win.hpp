#pragma once

#include "Common.hpp"
#include "exceptions/LibraryLoaderException.hpp"

class LibraryLoader_Win {

public:

	LibraryLoader_Win();
	~LibraryLoader_Win();

	virtual bool load(std::string fileName);

	template <typename T>
	std::function<T> resolveFunction(const std::string& funcName) {
		FARPROC lpfnGetProcessID = GetProcAddress(_procIdll, funcName.c_str());

		if (!lpfnGetProcessID) {
			throw LibraryLoaderException("Could not locate the function " + funcName);
		}

		return reinterpret_cast<T*>(lpfnGetProcessID);
	}

private:
	LibraryLoader_Win& operator=(const LibraryLoader_Win&);
	LibraryLoader_Win(const LibraryLoader_Win&);
	HINSTANCE _procIdll;

};