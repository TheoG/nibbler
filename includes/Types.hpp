#pragma once

#include "Vector.hpp"

enum Direction {
	North,
	East,
	South,
	West
};

enum Difficulty {
	Easy,
	Normal,
	Hard
};

enum GameMode {
	Solo,
	LocalMultiplayer,
	NetworkMultiplayerHost,
	NetworkMultiplayerClient
};

enum NetworkData {
	InitBoard,
	GameState,
	NextDirection,
	Ready,
	Restart,
};

enum Block {
	Empty,
	Wall,
	Snake_P1,
	Snake_P2,
	Snake_Head_P1,
	Snake_Head_P2,
	Food,
	Bonus
};

enum Input {
	Nibbler_Escape,
	Nibbler_Space,

	Nibbler_Left,
	Nibbler_Up,
	Nibbler_Right,
	Nibbler_Down,

	Nibbler_A,
	Nibbler_W,
	Nibbler_D,
	Nibbler_S,

	Nibbler_R,

	Nibbler_1,
	Nibbler_2,
	Nibbler_3,
};


enum Sounds {
	Eat,
	Death,
	PowerUp,
	Music
};

enum Bonuses {
	None,

	SpeedDown,
	SizeDown,
	ScoreUp,

	SpeedUp,
	SizeUp,
	ScoreDown,

	BonusEnd
};

enum WinState {
	NoOne,
	Player1,
	Player2,
	Draw,
	Win,
	Lose
};

typedef struct NetworkGameData {
	iVector2 foodPosition;
	iVector2 bonusPosition;
	Direction snake1Direction;
	Direction snake2Direction;
	int snake1BodySize;
	int snake2BodySize;
	int snake1Score;
	int snake2Score;
	bool snake1Alive;
	bool snake2Alive;
	WinState winState;
	Sounds soundToPlay;
} NetworkGameData;

typedef struct GameData {

	const std::vector<Block>& blocks;
	double elapsedTime;
	int player1Score;
	int player2Score;
	Direction snake1Direction;
	Direction snake2Direction;
	bool player1Ready;
	bool player2Ready;
	WinState winner;
} GameData;
