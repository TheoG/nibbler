#pragma once

#include <iostream>
#include <vector>
#include <map>
#include <array>
#include <list>
#include <algorithm>
#include <chrono>
#include <memory>
#include <functional>
#include <future>
#include <thread>
#include <string>
#include <string.h>

#ifdef WIN32
 # include <WinSock2.h>
 # include <WS2tcpip.h>
 # include <io.h>
 # include <Windows.h>
 typedef int socklen_t;
#else
 # include <dlfcn.h>
#endif

#include "Vector.hpp"

#define SOCKET_ERROR -1
