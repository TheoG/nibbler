#pragma once

#include "Common.hpp"

#include "IRenderer.hpp"
#include "ISound.hpp"
#include "Board.hpp"
#include "Snake.hpp"
#include "Client.hpp"
#include "Server.hpp"
#include "NetworkSender.hpp"

#ifdef WIN32
# include "LibraryLoader_Win.hpp"
#else
# include "LibraryLoader_Unix.hpp"
#endif

#include "Logger.hpp"

#define SFML 0
#define SDL 1
#define NCURSES 2

class Game {

public:
	Game(iVector2 boardSize, GameMode gm = GameMode::Solo, int difficulty = 1, int level = 0);
	~Game();

	void updateGameState();
	void getEvents();
	void updateUI();

	void loadLibrary();
	void loadLibrary(int index);

	void loop();
	void start();
	void restart();
	void playSound(Sounds sound);

	void resize(iVector2 boardSize);

	std::shared_ptr<Board> getBoard();
	IRenderer* getCurrentRenderer();

	const WinState& getWinState() const;
	void setWinState(WinState winState);

	void setServerAddress(const std::string& address);
	void setServerPort(int port);
	void launchServer();
	void launchClient();

	std::mutex loopMutex;

	static const float DefaultTickDuration;
private:

	Game(const Game&);
	Game& operator=(const Game&);

	std::shared_ptr<Board> _board;
	std::shared_ptr<Snake> _snake1;
	std::shared_ptr<Snake> _snake2;

	#ifdef WIN32
	std::vector<LibraryLoader_Win*> _loaders;
	#else
	std::vector<LibraryLoader_Unix*> _loaders;
	#endif

	std::chrono::high_resolution_clock::time_point _startTime;
	std::vector<IRenderer*> _renderers;
	IRenderer* _currentRenderer;
	ISound* _soundSystem;
	GameMode _gameMode;

	double _endTime;
	double _elapsedTime;

	bool _shouldRun;
	bool _started;
	WinState _winner;

	int _currentLibraryIndex;
	int _difficulty;


	// Network
	std::chrono::high_resolution_clock::time_point _connectionTime;
	std::shared_ptr<NetworkSender> _sender;
	std::string _serverAddress;
	int _serverPort;
	Client* _client;
	Server* _server;
	double _connectionTimeout;
	double _connectionElaspedTime;


	// Load dlls
	void _loadLibraries(iVector2 boardSize);

	static const std::map<Input, Direction> _InputToDirection;
	static const std::map<Direction, Direction> _OpposedDirection;
};
