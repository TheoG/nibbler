#pragma once

#include <exception>
#include <string>

class LibraryLoaderException: public std::exception {

public:
	LibraryLoaderException( void );
	LibraryLoaderException( const std::string& errorMessage );
	LibraryLoaderException( LibraryLoaderException const & );
	~LibraryLoaderException( void );

	virtual const char* what() const throw();

	LibraryLoaderException & operator=( LibraryLoaderException const & rhs );
private:
	std::string _errorMessage;
};