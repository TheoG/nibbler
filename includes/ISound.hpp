#pragma once

#include "Types.hpp"

class ISound {

public:

	virtual void init() = 0;
	virtual void deinit() = 0;

	virtual void play(const Sounds&) = 0;
	virtual void stop(const Sounds&) = 0;
};
