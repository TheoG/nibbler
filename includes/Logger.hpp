#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

class Logger final {

public:

	Logger();
	~Logger();

	static void setFileLog(bool fileLog);
	static std::ostream& os;
	static Logger Log;

	template<class T>
	Logger& operator<<(T&& output)
	{
		if (Logger::_fileLog) {
			if (_file.good()) {
				_file << output << std::endl;
			}
		} else {
			std::cout << output;
		}
	return *this;
	}

private:

	Logger& operator=(const Logger&);
	Logger(const Logger&);

	static bool _fileLog;
	static std::ofstream _file;
};
