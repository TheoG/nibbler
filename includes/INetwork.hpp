#pragma once

class INetwork {

public:
	virtual bool openConnection() = 0;
	virtual void sendData(const char* data, int length) = 0;
	virtual std::string receiveData() = 0;
};