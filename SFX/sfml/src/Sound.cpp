#include "Sound.hpp"

Sound::Sound() {}

Sound::~Sound() {}

void Sound::init() {
	if (!_buffers[Sounds::Eat].loadFromFile("./resources/sounds/eat.wav"))
		return ;
	_sounds[Sounds::Eat].setBuffer(_buffers[Sounds::Eat]);
	if (!_buffers[Sounds::Death].loadFromFile("./resources/sounds/death.wav"))
		return ;
	_sounds[Sounds::Death].setBuffer(_buffers[Sounds::Death]);
	if (!_buffers[Sounds::PowerUp].loadFromFile("./resources/sounds/powerup.wav"))
		return ;
	_sounds[Sounds::PowerUp].setBuffer(_buffers[Sounds::PowerUp]);
	if (!_music.openFromFile("./resources/sounds/music_loop.wav"))
		return ;
	_music.setLoop(true);
	_music.setVolume(25);
	_sounds[Sounds::Eat].setVolume(100);
	_sounds[Sounds::Death].setVolume(100);
}

void Sound::deinit() {
}

void Sound::play(const Sounds& sound) {
	if (sound == Sounds::Music) {
		_music.stop();
		_music.play();
	}
	else
		_sounds[sound].play();
}

void Sound::stop(const Sounds& sound) {
	if (sound == Sounds::Music) {
		_music.stop();
	}
	else
		_sounds[sound].stop();
}

extern "C" {

	#ifdef WIN32
	DECLDIR ISound* createSound() {
		return new Sound();
	}

	DECLDIR void deleteSound(ISound* sound) {
		delete reinterpret_cast<Sound*>(sound);
	}
	#else
	ISound* createSound() {
		return new Sound();
	}

	void deleteSound(ISound* sound) {
		delete reinterpret_cast<Sound*>(sound);
	}
	#endif

}
