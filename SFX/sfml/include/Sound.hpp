#pragma once

#include <SFML/Audio.hpp>
#include <map>
#include <iostream>
#include "ISound.hpp"

#ifdef WIN32
 # if defined DLL_EXPORT
   # define DECLDIR __declspec(dllexport)
 #else
  # define DECLDIR __declspec(dllimport)
 #endif
#endif

class Sound final: public ISound {

public:

	Sound();
	~Sound();

	void init();
	void deinit();

	void play(const Sounds&);
	void stop(const Sounds&);

private:

	Sound& operator=(const Sound&);
	Sound(const Sound&);

	std::map<Sounds, sf::Sound> _sounds;
	std::map<Sounds, sf::SoundBuffer> _buffers;
	sf::Music _music;

};

extern "C" {
	#ifdef WIN32
	DECLDIR ISound* createSound();
	DECLDIR void deleteSound(ISound* sound);
	#else
	ISound* createSound();
	void deleteSound(ISound* sound);
	#endif
}
