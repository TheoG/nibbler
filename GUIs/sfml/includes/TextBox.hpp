#pragma once

#include "IRenderable.hpp"
#include "Logger.hpp"

class TextBox : public IRenderable {

public:
	TextBox(sf::Vector2f size, sf::Vector2f position, sf::Color backgroundColor);

	void render(sf::RenderWindow&);
	void click(sf::Vector2i mousePosition);
	void hover(sf::Vector2i mousePosition);

	void setText(const std::string& text, bool center = true);
	void setFontColor(sf::Color color);
	void setFontSize(unsigned fontSize);

	void rotate(double angle);

	static void loadFont();
	static void unloadFont();
	static sf::Font* font;

private:

	TextBox();
	TextBox(const TextBox&);
	TextBox& operator=(const TextBox&);

	sf::Text _text;
};
