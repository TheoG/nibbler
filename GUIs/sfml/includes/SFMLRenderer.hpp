#pragma once

#include <map>
#include "IRenderer.hpp"
#include <SFML/Window.hpp>
#include "GUI.hpp"
#include "Vector.hpp"
#include "Logger.hpp"

#ifdef WIN32
 # if defined DLL_EXPORT
   # define DECLDIR __declspec(dllexport)
 #else
  # define DECLDIR __declspec(dllimport)
 #endif
#endif

class SFMLRenderer final: public IRenderer {

public:

	SFMLRenderer(iVector2 gameAreaSize);
	~SFMLRenderer();

	void init();
	void deinit();

	std::vector<Input> getEvents();
	void updateUI(const GameData&);

	void resize(iVector2 newSize);


private:

	SFMLRenderer();
	SFMLRenderer(const SFMLRenderer&);
	SFMLRenderer& operator=(const SFMLRenderer&);

	sf::RenderWindow _window;
	static const std::map<sf::Keyboard::Key, Input> _InputTranslationTable;
	static const int _DefaultBlockSize;
	
	static const std::map<Block, std::function<void(const GameData& gameData, GUI*, sf::Sprite&)>> _TextureBlockFunction;

	GUI* _gui;
	iVector2 _gameAreaSize;
	int _blockSize;
	bool _shouldResize;
};


extern "C" {
	#ifdef WIN32
	DECLDIR IRenderer* createSFMLRenderer(iVector2 gameAreaSize);
	DECLDIR void deleteSFMLRenderer(IRenderer* renderer);
	#else
	IRenderer* createSFMLRenderer(iVector2 gameAreaSize);
	void deleteSFMLRenderer(IRenderer* renderer);
	#endif
}
