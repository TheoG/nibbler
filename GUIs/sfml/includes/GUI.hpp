#pragma once

#include "Background.hpp"
#include "TextBox.hpp"
#include "Vector.hpp"
#include "Types.hpp"

class GUI {

public:
	GUI();
	~GUI();

	void setup(iVector2 screenSize);

	const std::vector<std::shared_ptr<IRenderable>>& getRenderables() const;

	void updateTimer(double elapsedTime);
	void updatePlayer1Score(int score, bool ready);
	void updatePlayer2Score(int score, bool ready);
	void updateVictoryMessage(WinState winner);
	std::shared_ptr<IRenderable> getVictoryText();

	const sf::Texture& wallTexture() const;
	const sf::Texture& fruitTexture() const;
	const sf::Texture& snakeBodyTexture() const;
	const sf::Texture& snakeHeadTexture() const;
	const sf::Texture& bonusTexture() const;

private:

	GUI(const GUI&);
	GUI& operator=(const GUI&);

	std::shared_ptr<Background> _background;
	std::shared_ptr<TextBox> _timer;
	std::shared_ptr<TextBox> _player1Score;
	std::shared_ptr<TextBox> _player2Score;
	std::shared_ptr<TextBox> _victoryMessage;

	sf::Texture _wallTexture;
	sf::Texture _fruitTexture;
	sf::Texture _snakeBodyTexture;
	sf::Texture _snakeHeadTexture;
	sf::Texture _bonusTexture;

	std::vector<std::shared_ptr<IRenderable>> _renderables;
};