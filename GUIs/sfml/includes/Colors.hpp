#pragma once

#include <SFML/Graphics.hpp>

struct Colors {
	static const sf::Color White;
	static const sf::Color Black;
	static const sf::Color Snake_P1;
	static const sf::Color Snake_P2;
	static const sf::Color Timer;
	static const sf::Color Transparent;
};
