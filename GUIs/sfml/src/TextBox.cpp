#include "TextBox.hpp"

#include <iostream>

TextBox::TextBox(sf::Vector2f size, sf::Vector2f position, sf::Color backgroundColor) {

	_position = position;
	_size = size;

	_shape = std::make_shared<sf::RectangleShape>(size);
	_shape->setPosition(position);
	_shape->setFillColor(backgroundColor);

	_text.setFont(*TextBox::font);

	sf::FloatRect textRect = _text.getLocalBounds();
	_text.setOrigin(textRect.left + textRect.width / 2.0f, textRect.top + textRect.height / 2.0f);
	_text.setPosition(sf::Vector2f(position.x + size.x / 2.0f, position.y + size.y / 2.0f));

	_text.setPosition(position.x, position.y + size.y / 2.0f - 25 / 2.0f);
	_text.setCharacterSize(25);
}

void TextBox::render(sf::RenderWindow& window) {
	window.draw(*_shape);
	window.draw(_text);
}

void TextBox::click(sf::Vector2i mousePosition) {
	(void)mousePosition;
	return;
}

void TextBox::hover(sf::Vector2i mousePosition) {
	(void)mousePosition;
}

void TextBox::setFontColor(sf::Color color) {
	_text.setFillColor(color);
}

void TextBox::setText(const std::string& buttonText, bool center) {
	_text.setString(buttonText);
	if (center) {
		sf::FloatRect textRect = _text.getLocalBounds();
		_text.setOrigin(textRect.left + textRect.width / 2.0f, textRect.top + textRect.height / 2.0f);
		_text.setPosition(sf::Vector2f(_position.x + _size.x / 2.0f, _position.y + _size.y / 2.0f));
	}
}

void TextBox::setFontSize(unsigned fontSize) {
	_text.setCharacterSize(fontSize);
	sf::FloatRect textRect = _text.getLocalBounds();
	_text.setOrigin(textRect.left + textRect.width / 2.0f, textRect.top + textRect.height / 2.0f);
	_text.setPosition(sf::Vector2f(_position.x + _size.x / 2.0f, _position.y + _size.y / 2.0f));
}

void TextBox::rotate(double angle) {
	_text.rotate(angle);
}

sf::Font* TextBox::font;

void TextBox::loadFont() {
	TextBox::font = new sf::Font();
	if (!TextBox::font->loadFromFile("./resources/fonts/Chilanka-Regular.ttf")) {
		std::cerr << "Error while loading font" << std::endl;
	}
}

void TextBox::unloadFont() {
	delete TextBox::font;
	TextBox::font = nullptr;
}

TextBox::TextBox() {}

TextBox::TextBox(const TextBox& other) {
	*this = other;
}

TextBox& TextBox::operator=(const TextBox& other) {
	_position = other._position;
	_size = other._size;
	_text = other._text;
	return *this;
}