#include "GUI.hpp"

#include "TextBox.hpp"
#include "Colors.hpp"

#include <iostream>
#include <sstream>
#include <iomanip>

GUI::GUI() {
	TextBox::loadFont();
}

GUI::~GUI() {
	TextBox::unloadFont();
}

GUI::GUI(const GUI&) {}
GUI& GUI::operator=(const GUI&) {
	return *this;
}

void GUI::setup(iVector2 screenSize) {

	_background = std::make_shared<Background>(sf::Vector2f(screenSize.x, screenSize.y), sf::Vector2f(0.0f, 100.0f), Colors::White);
	_background->setTexture("resources/textures/background.png");

	_timer = std::make_shared<TextBox>(sf::Vector2f(0.0f, 0.0f), sf::Vector2f(screenSize.x / 2, 75.0f), sf::Color::Transparent);
	_timer->setText("00:00");
	_timer->setFontColor(Colors::Timer);
	_timer->setFontSize(50);

	_player1Score = std::make_shared<TextBox>(sf::Vector2f(0.0f, 0.0f), sf::Vector2f(100.0f, 75.0f), sf::Color::Transparent);
	_player1Score->setText("00000");
	_player1Score->setFontColor(Colors::Snake_P1);
	_player1Score->setFontSize(40);

	_player2Score = std::make_shared<TextBox>(sf::Vector2f(0.0f, 0.0f), sf::Vector2f(screenSize.x - 100, 75.0f), sf::Color::Transparent);
	_player2Score->setText("00000");
	_player2Score->setFontColor(Colors::Snake_P2);
	_player2Score->setFontSize(40);

	_victoryMessage = std::make_shared<TextBox>(sf::Vector2f(0.0f, 0.0f), sf::Vector2f(screenSize.x / 2, screenSize.y / 2 + 25), sf::Color::Transparent);
	_victoryMessage->setText("");
	_victoryMessage->setFontColor(Colors::White);
	_victoryMessage->setFontSize(50);
	_victoryMessage->rotate(-25);

	_renderables.push_back(_background);
	_renderables.push_back(_timer);
	_renderables.push_back(_player1Score);
	_renderables.push_back(_player2Score);

	if (!_wallTexture.loadFromFile("resources/textures/wall.png")) {
		Logger::Log << "Error with wall.png\n";
	}
	if (!_fruitTexture.loadFromFile("resources/textures/fruit.png")) {
		Logger::Log << "Error with fruit.png\n";
	}
	if (!_snakeBodyTexture.loadFromFile("resources/textures/snakebody.png")) {
		Logger::Log << "Error with snakebody.png\n";
	}
	if (!_snakeHeadTexture.loadFromFile("resources/textures/snakehead.png")) {
		Logger::Log << "Error with snakehead.png\n";
	}
	if (!_bonusTexture.loadFromFile("resources/textures/bonus.png")) {
		Logger::Log << "Error with bonus.png\n";
	}
}

const std::vector<std::shared_ptr<IRenderable>>& GUI::getRenderables() const {
	return _renderables;
}

void GUI::updateTimer(double elapsedTime) {
	std::stringstream ss;
	int minutes = (elapsedTime / 60);
	int seconds = static_cast<int>(elapsedTime) % 60;

	ss << (minutes < 10 ? "0" : "") << minutes << ":" << (seconds < 10 ? "0" : "") << seconds;
	_timer->setText(ss.str(), false);
}

void GUI::updatePlayer1Score(int score, bool ready) {
	std::stringstream ss;
	ss << std::setfill('0') << std::setw(5) << score;
	_player1Score->setText(ss.str());
	(void)ready;
	_player1Score->setFontColor(sf::Color(sf::Color::Yellow.r, sf::Color::Yellow.g, sf::Color::Yellow.b, ready ? 255 : 100));
}

void GUI::updatePlayer2Score(int score, bool ready) {
	std::stringstream ss;
	if (score == -1) {
		ss << "";
	} else {
		ss << std::setfill('0') << std::setw(5) << score;
	}
	_player2Score->setText(ss.str());
	_player2Score->setFontColor(sf::Color(sf::Color::Blue.r, sf::Color::Blue.g, sf::Color::Blue.b, ready ? 255 : 100));
}

void GUI::updateVictoryMessage(WinState winner) {
	switch (winner) {
	case WinState::NoOne:
		_victoryMessage->setText("");
		break;
	case WinState::Player1:
		_victoryMessage->setText("Player 1 win!");
		break;
	case WinState::Player2:
		_victoryMessage->setText("Player 2 win!");
		break;
	case WinState::Draw:
		_victoryMessage->setText("Draw...");
		break;
	case WinState::Win:
		_victoryMessage->setText("You win!");
		break;
	case WinState::Lose:
		_victoryMessage->setText("You lose...!");
		break;
	}
}

std::shared_ptr<IRenderable> GUI::getVictoryText() {
	return _victoryMessage;
}


const sf::Texture& GUI::wallTexture() const {
	return _wallTexture;
}

const sf::Texture& GUI::fruitTexture() const {
	return _fruitTexture;
}

const sf::Texture& GUI::snakeBodyTexture() const {
	return _snakeBodyTexture;
}

const sf::Texture& GUI::snakeHeadTexture() const {
	return _snakeHeadTexture;
}

const sf::Texture& GUI::bonusTexture() const {
	return _bonusTexture;
}
