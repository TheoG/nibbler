#include "Colors.hpp"

const sf::Color Colors::White = sf::Color(255, 255, 255);
const sf::Color Colors::Black = sf::Color(0, 0, 0);
const sf::Color Colors::Snake_P1 = sf::Color(0, 200, 20);
const sf::Color Colors::Snake_P2 = sf::Color(76, 76, 255);
const sf::Color Colors::Timer = sf::Color(220, 220, 0);
const sf::Color Colors::Transparent = sf::Color(255, 255, 255, 255);
