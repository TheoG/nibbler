#include "IRenderable.hpp"

void IRenderable::setBorder(float thickness, sf::Color color) {
	_shape->setOutlineThickness(thickness);
	_shape->setOutlineColor(color);
}

void IRenderable::setBackgroundColor(sf::Color color) {
	_shape->setFillColor(color);
}