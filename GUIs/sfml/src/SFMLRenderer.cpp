#include "SFMLRenderer.hpp"
#include "Colors.hpp"

SFMLRenderer::SFMLRenderer(iVector2 gameAreaSize): _gameAreaSize(gameAreaSize), _blockSize(SFMLRenderer::_DefaultBlockSize), _shouldResize(false) {
	Logger::Log << "SFML Renderer Constructor\n";
}

SFMLRenderer::~SFMLRenderer() {
	Logger::Log << "SFML Renderer Destructor\n";
}

SFMLRenderer::SFMLRenderer() {}
SFMLRenderer::SFMLRenderer(const SFMLRenderer& other) {
	*this = other;
}

SFMLRenderer& SFMLRenderer::operator=(const SFMLRenderer&) {
	return *this;
}

void SFMLRenderer::init() {
	Logger::setFileLog(false);
	Logger::Log << "SFMLRenderer::Init\n";

	_gui = new GUI();

	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

	_window.create(sf::VideoMode(_gameAreaSize.x * _blockSize, _gameAreaSize.y * _blockSize + 100), "Nibbler - SFML", sf::Style::Titlebar | sf::Style::Close, settings);
	_window.requestFocus();
	_gui->setup(iVector2(_gameAreaSize.x * _blockSize, _gameAreaSize.y * _blockSize + 100));
}

void SFMLRenderer::deinit() {
	Logger::Log << "SFMLRenderer::Deinit\n";
	_window.close();
	delete _gui;
}

std::vector<Input> SFMLRenderer::getEvents() {
	std::vector<Input> inputs;

	sf::Event event;
	while (_window.pollEvent(event)) {
		if (event.type == sf::Event::Closed) {
			inputs.push_back(Input::Nibbler_Escape);
		}
		if (event.type == sf::Event::KeyPressed) {
			auto it = SFMLRenderer::_InputTranslationTable.find(event.key.code);
			if (it != SFMLRenderer::_InputTranslationTable.end()) {
				inputs.push_back(it->second);
			}
		}
	}

	return inputs;
}

void SFMLRenderer::updateUI(const GameData& data) {
	if (_shouldResize) {
		_shouldResize = false;
		_window.close();
		_window.create(sf::VideoMode(_gameAreaSize.x * _blockSize, _gameAreaSize.y * _blockSize + 100), "Nibbler", sf::Style::Titlebar | sf::Style::Close);
	}

	_window.clear(sf::Color::Black);

	auto renderables = _gui->getRenderables();
	for (auto renderable : renderables) {
		renderable->render(_window);
	}

	for (int y = 0; y < _gameAreaSize.y; ++y) {
		for (int x = 0; x < _gameAreaSize.x; ++x) {
			sf::Sprite rec;

			rec.scale(0.1f, 0.1f);
			rec.setPosition(sf::Vector2f(_blockSize / 2 + _blockSize * x, _blockSize / 2 + _blockSize * y + 100));

			Block b = data.blocks[y * _gameAreaSize.x + x];

			rec.setOrigin(250, 250);

			auto it = _TextureBlockFunction.find(b);

			if (it != _TextureBlockFunction.end()) {
				it->second(data, _gui, rec);
			} else {
				continue;
			}

			_window.draw(rec);
		}
	}

	_gui->updateTimer(data.elapsedTime);
	_gui->updatePlayer1Score(data.player1Score, data.player1Ready);
	_gui->updatePlayer2Score(data.player2Score, data.player2Ready);
	_gui->updateVictoryMessage(data.winner);
	_gui->getVictoryText()->render(_window);

	_window.display();
}

void SFMLRenderer::resize(iVector2 newSize) {
	_gameAreaSize = newSize;
	_shouldResize = true;
}

extern "C" {

	#ifdef WIN32
	DECLDIR IRenderer* createSFMLRenderer(iVector2 gameAreaSize) {
		return new SFMLRenderer(gameAreaSize);
	}

	DECLDIR void deleteSFMLRenderer(IRenderer* sfmlRenderer) {
		delete sfmlRenderer;
	}
	#else
	IRenderer* createSFMLRenderer(iVector2 gameAreaSize) {
		return new SFMLRenderer(gameAreaSize);
	}

	void deleteSFMLRenderer(IRenderer* sfmlRenderer) {
		delete reinterpret_cast<SFMLRenderer*>(sfmlRenderer);
	}
	#endif

}

const std::map<sf::Keyboard::Key, Input> SFMLRenderer::_InputTranslationTable = {
	{ sf::Keyboard::Escape, Input::Nibbler_Escape },
	{ sf::Keyboard::Space, Input::Nibbler_Space },
	
	{ sf::Keyboard::Down, Input::Nibbler_Down },
	{ sf::Keyboard::Up, Input::Nibbler_Up },
	{ sf::Keyboard::Right, Input::Nibbler_Right },
	{ sf::Keyboard::Left, Input::Nibbler_Left },

	{ sf::Keyboard::A, Input::Nibbler_A },
	{ sf::Keyboard::W, Input::Nibbler_W },
	{ sf::Keyboard::D, Input::Nibbler_D },
	{ sf::Keyboard::S, Input::Nibbler_S },

	{ sf::Keyboard::R, Input::Nibbler_R },

	{ sf::Keyboard::Num1, Input::Nibbler_1 },
	{ sf::Keyboard::Num2, Input::Nibbler_2 },
	{ sf::Keyboard::Num3, Input::Nibbler_3 },
	
	{ sf::Keyboard::F1, Input::Nibbler_1 },
	{ sf::Keyboard::F2, Input::Nibbler_2 },
	{ sf::Keyboard::F3, Input::Nibbler_3 },
};

const int SFMLRenderer::_DefaultBlockSize = 50;

const std::map<Block, std::function<void(const GameData& gameData, GUI*, sf::Sprite&)>> SFMLRenderer::_TextureBlockFunction = {
	{
		Block::Food, [](const GameData&, GUI* gui, sf::Sprite& sprite) {
			sprite.setTexture(gui->fruitTexture());
		}
	},
	{
		Block::Bonus, [](const GameData&, GUI* gui, sf::Sprite& sprite) {
			sprite.setTexture(gui->bonusTexture());
		}
	},
	{
		Block::Wall, [](const GameData&, GUI* gui, sf::Sprite& sprite) {
			sprite.setTexture(gui->wallTexture());
		}
	},
	{
		Block::Snake_P1, [](const GameData&, GUI* gui, sf::Sprite& sprite) {
			sprite.setTexture(gui->snakeBodyTexture());
			sprite.setColor(Colors::Snake_P1);
		}
	},
	{
		Block::Snake_Head_P1, [](const GameData& data, GUI* gui, sf::Sprite& sprite) {
			sprite.setTexture(gui->snakeHeadTexture());
			sprite.setColor(Colors::Snake_P1);
			if (data.snake1Direction == Direction::South) {
				sprite.rotate(90);
			} else if (data.snake1Direction == Direction::West) {
				sprite.rotate(180);
			} else if (data.snake1Direction == Direction::North) {
				sprite.rotate(270);
			}
		}
	},
	{
		Block::Snake_P2, [](const GameData&, GUI* gui, sf::Sprite& sprite) {
			sprite.setTexture(gui->snakeBodyTexture());
			sprite.setColor(Colors::Snake_P2);
		}
	},
	{
		Block::Snake_Head_P2, [](const GameData& data, GUI* gui, sf::Sprite& sprite) {
			sprite.setTexture(gui->snakeHeadTexture());
			sprite.setColor(Colors::Snake_P2);
			if (data.snake2Direction == Direction::South) {
				sprite.rotate(90);
			} else if (data.snake2Direction == Direction::West) {
				sprite.rotate(180);
			} else if (data.snake2Direction == Direction::North) {
				sprite.rotate(270);
			}
		}
	},
};
