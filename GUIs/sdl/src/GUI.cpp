#include "GUI.hpp"

#include <iostream>
#include <sstream>
#include <iomanip>

GUI::GUI(SDL_Renderer* renderer): _renderer(renderer) {
	TextBox::loadFont();
	_wallTexture = _fruitTexture = _snakeBodyTexture = _snakeHeadTexture = _bonusTexture = nullptr;
}

GUI::~GUI() {
	TextBox::unloadFont();
	if (_wallTexture) {
		SDL_DestroyTexture(_wallTexture);
	}
	if (_fruitTexture) {
		SDL_DestroyTexture(_fruitTexture);
	}
	if (_snakeBodyTexture) {
		SDL_DestroyTexture(_snakeBodyTexture);
	}
	if (_snakeHeadTexture) {
		SDL_DestroyTexture(_snakeHeadTexture);
	}
	if (_bonusTexture) {
		SDL_DestroyTexture(_bonusTexture);
	}
	_renderables.clear();
}

GUI::GUI(const GUI&) {}
GUI& GUI::operator=(const GUI&) {
	return *this;
}

void GUI::setup(iVector2 screenSize) {

	SDL_Rect dimensions;
	dimensions.w = screenSize.x;
	dimensions.h = screenSize.y;
	dimensions.x = 0.0f;
	dimensions.y = 100.0f;

	_background = std::make_shared<Background>(dimensions, Colors::White);
	_background->setTexture(_renderer, "resources/textures/background.png");
	dimensions.x = screenSize.x / 2;
	dimensions.y = 60;
	_timer = std::make_shared<TextBox>(dimensions, Colors::Timer);
	_timer->setText("00:00", _renderer, 50);

	dimensions.x = 100;
	dimensions.y = 60;
	_player1Score = std::make_shared<TextBox>(dimensions, Colors::Snake_P1);
	_player1Score->setText("00000", _renderer, 25);

	dimensions.x = screenSize.x - 100;
	dimensions.y = 60;
	_player2Score = std::make_shared<TextBox>(dimensions, Colors::Snake_P2);
	_player2Score->setText("00000", _renderer, 25);

	dimensions.x = screenSize.x / 2;
	dimensions.y = screenSize.y / 2 + 50;
	_victoryMessage = std::make_shared<TextBox>(dimensions, Colors::Transparent);
	_victoryMessage->setText(" ", _renderer, 50);

	_renderables.push_back(_background);
	_renderables.push_back(_timer);
	_renderables.push_back(_player1Score);
	_renderables.push_back(_player2Score);

	auto createTexture = [this](const std::string& pathToTexture, SDL_Texture*& texture) {
		SDL_Surface* image;
		SDL_RWops* rwop;
		rwop = SDL_RWFromFile(pathToTexture.c_str(), "rb");
		image = IMG_LoadPNG_RW(rwop);
		if (!image) {
			Logger::Log << "Failed to load png (" << pathToTexture.c_str() << "): " <<  IMG_GetError() << "\n";
			return;
		}
		texture = SDL_CreateTextureFromSurface(_renderer, image);
		SDL_FreeSurface(image);
		SDL_RWclose(rwop);
	};
	createTexture("resources/textures/wall2.png", _wallTexture);
	createTexture("resources/textures/fruit2.png", _fruitTexture);
	createTexture("resources/textures/snakeBody2.png", _snakeBodyTexture);
	createTexture("resources/textures/snakeHead2.png", _snakeHeadTexture);
	createTexture("resources/textures/bonus2.png", _bonusTexture);
}

const std::vector<std::shared_ptr<IRenderable>>& GUI::getRenderables() const {
	return _renderables;
}

void GUI::updateTimer(double elapsedTime) {
	std::stringstream ss;
	int minutes = (elapsedTime / 60);
	int seconds = static_cast<int>(elapsedTime) % 60;

	ss << (minutes < 10 ? "0" : "") << minutes << ":" << (seconds < 10 ? "0" : "") << seconds;
	_timer->setText(ss.str(), _renderer, 50, false);
}

void GUI::updatePlayer1Score(int score, bool ready) {
	std::stringstream ss;
	ss << std::setfill('0') << std::setw(5) << score;
	SDL_Color color = Colors::Snake_P1;
	color.a = static_cast<Uint8>(ready ? 255 : 150);
	_player1Score->setText(ss.str(), _renderer, 25, color, false);
}

void GUI::updatePlayer2Score(int score, bool ready) {
	_player2Score->setEnabled(score >= 0);
	std::stringstream ss;
	ss << std::setfill('0') << std::setw(5) << score;
	SDL_Color color = Colors::Snake_P2;
	color.a = static_cast<Uint8>(ready ? 255 : 150);
	_player2Score->setText(ss.str(), _renderer, 25, color, false);
}

void GUI::updateVictoryMessage(WinState winner) {
	switch (winner) {
	case WinState::NoOne:
		_victoryMessage->setText(" ", _renderer, 50, Colors::White, true);
		break;
	case WinState::Player1:
		_victoryMessage->setText("Player 1 win!", _renderer, 50, Colors::White, true);
		break;
	case WinState::Player2:
		_victoryMessage->setText("Player 2 win!", _renderer, 50, Colors::White, true);
		break;
	case WinState::Draw:
		_victoryMessage->setText("Draw...", _renderer, 50, Colors::White, true);
		break;
	case WinState::Win:
		_victoryMessage->setText("You win!", _renderer, 50, Colors::White, true);
		break;
	case WinState::Lose:
		_victoryMessage->setText("You lose...!", _renderer, 50, Colors::White, true);
		break;
	}
}

std::shared_ptr<IRenderable> GUI::getVictoryText() {
	return _victoryMessage;
}

SDL_Texture* GUI::wallTexture() const {
	return _wallTexture;
}

SDL_Texture* GUI::fruitTexture() const {
	return _fruitTexture;
}

SDL_Texture* GUI::snakeBodyTexture() const {
	return _snakeBodyTexture;
}

SDL_Texture* GUI::snakeHeadTexture() const {
	return _snakeHeadTexture;
}

SDL_Texture* GUI::bonusTexture() const {
	return _bonusTexture;
}
