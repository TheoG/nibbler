#include "Colors.hpp"

const SDL_Color Colors::Transparent = { 0, 0, 0, 255 };
const SDL_Color Colors::White = { 255, 255, 255, 0 };
const SDL_Color Colors::Black = { 0, 0, 0, 0 };
const SDL_Color Colors::Snake_P1 = { 0, 200, 20, 0 };
const SDL_Color Colors::Snake_P2 = { 76, 76, 255, 0 };
const SDL_Color Colors::Timer = { 220, 220, 0, 0 };
