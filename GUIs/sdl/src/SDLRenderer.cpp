#include "SDLRenderer.hpp"

SDLRenderer::SDLRenderer(iVector2 gameAreaSize): _window(nullptr), _gameAreaSize(gameAreaSize), _blockSize(SDLRenderer::_DefaultBlockSize), _shouldResize(false) {
	Logger::Log << "SDL Renderer Constructor\n";
	_blocksTextures = std::vector<std::shared_ptr<Background>>(gameAreaSize.x * gameAreaSize.y, nullptr);
	for (int y = 0; y < _gameAreaSize.y; ++y) {
		for (int x = 0; x < _gameAreaSize.x; ++x) {
			SDL_Rect rect;

			rect.h = rect.w = 50;
			rect.x = _blockSize * x;
			rect.y = _blockSize * y + 100;

			_blocksTextures[y * _gameAreaSize.x + x] = std::make_unique<Background>(rect, Colors::White);
		}
	}
}

SDLRenderer::~SDLRenderer() {
	Logger::Log << "SDL Renderer Destructor\n";
	SDL_DestroyWindow(_window);
}

SDLRenderer::SDLRenderer() {}
SDLRenderer::SDLRenderer(const SDLRenderer& other) {
	*this = other;
}

SDLRenderer& SDLRenderer::operator=(const SDLRenderer&) {
	return *this;
}

void SDLRenderer::init() {
	Logger::setFileLog(false);
	Logger::Log << "SDLRenderer::Init\n";

	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		Logger::Log << "Failed to init SDL: " << SDL_GetError() << "\n";
		std::exit(EXIT_FAILURE);
    }

	if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG)) {
		Logger::Log << "Failed to init SDL_image" << IMG_GetError() << "\n";
		std::exit(EXIT_FAILURE);
	}

	if (TTF_Init() == -1) {
		Logger::Log << "Failed to init SDL_ttf" << TTF_GetError() << "\n";
		std::exit(EXIT_FAILURE);
	}
	
	_window = SDL_CreateWindow("Nibbler - SDL",  SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _gameAreaSize.x * _blockSize, _gameAreaSize.y * _blockSize + 100, SDL_WINDOW_SHOWN);

	if (!_window) {
		Logger::Log << "Failed to create the window: " << SDL_GetError() << "\n";
		std::exit(EXIT_FAILURE);
	}

	_renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (!_renderer) {
		Logger::Log << "Failed to create the renderer: " << SDL_GetError() << "\n";
		std::exit(EXIT_FAILURE);
	}
	_gui = new GUI(_renderer);
	_gui->setup(iVector2(_gameAreaSize.x * _blockSize, _gameAreaSize.y * _blockSize + 100));
}

void SDLRenderer::deinit() {
	Logger::setFileLog(false);
	Logger::Log << "SDLRenderer::Deinit\n";
	if (_gui) {
		delete _gui;
	}
	if (_window) {
		SDL_DestroyWindow(_window);
	}
	SDL_Quit();
	IMG_Quit();
	TTF_Quit();
}

std::vector<Input> SDLRenderer::getEvents() {
	std::vector<Input> inputs;


	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		if (event.type == SDL_QUIT) {
			inputs.push_back(Input::Nibbler_Escape);
		} else if (event.type == SDL_KEYDOWN) {
			auto it = SDLRenderer::_InputTranslationTable.find(event.key.keysym.sym);
			if (it != SDLRenderer::_InputTranslationTable.end()) {
				inputs.push_back(it->second);
			}
		}
	}
	return inputs;
}

void SDLRenderer::updateUI(const GameData& data) {
	(void)data;
	if (_shouldResize) {
		_shouldResize = false;
		SDL_SetWindowSize(_window, _gameAreaSize.x * _blockSize, _gameAreaSize.y * _blockSize + 100);
		_blocksTextures.clear();
		_blocksTextures = std::vector<std::shared_ptr<Background>>(_gameAreaSize.x * _gameAreaSize.y, nullptr);
		for (int y = 0; y < _gameAreaSize.y; ++y) {
			for (int x = 0; x < _gameAreaSize.x; ++x) {
				SDL_Rect rect;

				rect.h = rect.w = 50;
				rect.x = _blockSize * x;
				rect.y = _blockSize * y + 100;

				_blocksTextures[y * _gameAreaSize.x + x] = std::make_unique<Background>(rect, Colors::White);
			}
		}
	}

	SDL_RenderClear(_renderer);

	auto renderables = _gui->getRenderables();
	for (auto renderable : renderables) {
		renderable->render(_renderer);
	}

	for (int y = 0; y < _gameAreaSize.y; ++y) {
		for (int x = 0; x < _gameAreaSize.x; ++x) {
	
			std::weak_ptr<Background> weakSprite = _blocksTextures[y * _gameAreaSize.x + x];
			if (weakSprite.expired()) {
				continue;
			}
			auto sprite = weakSprite.lock();
			sprite->setAngle(0.0);

			Block b = data.blocks[y * _gameAreaSize.x + x];

			auto it = _TextureBlockFunction.find(b);
			if (it != _TextureBlockFunction.end()) {
				it->second(data, _gui, sprite);
			} else {
				continue;
			}
			sprite->render(_renderer);
		}
	}

	_gui->updateTimer(data.elapsedTime);
	_gui->updatePlayer1Score(data.player1Score, data.player1Ready);
	_gui->updatePlayer2Score(data.player2Score, data.player2Ready);
	_gui->updateVictoryMessage(data.winner);
	_gui->getVictoryText()->render(_renderer);
	SDL_RenderPresent(_renderer);
}

void SDLRenderer::resize(iVector2 newSize) {
	_gameAreaSize = newSize;
	_shouldResize = true;
}

extern "C" {

	#ifdef WIN32
	DECLDIR IRenderer* createSDLRenderer(iVector2 gameAreaSize) {
		return new SDLRenderer(gameAreaSize);
	}

	DECLDIR void deleteSDLRenderer(IRenderer* sdlRenderer) {
		delete sdlRenderer;
	}
	#else
	IRenderer* createSDLRenderer(iVector2 gameAreaSize) {
		return new SDLRenderer(gameAreaSize);
	}

	void deleteSDLRenderer(IRenderer* sdlRenderer) {
		delete reinterpret_cast<SDLRenderer*>(sdlRenderer);
	}
	#endif

}

const std::map<SDL_Keycode, Input> SDLRenderer::_InputTranslationTable = {
	{ SDLK_ESCAPE, Input::Nibbler_Escape },
	{ SDLK_SPACE, Input::Nibbler_Space },
	
	{ SDLK_DOWN, Input::Nibbler_Down },
	{ SDLK_UP, Input::Nibbler_Up },
	{ SDLK_RIGHT, Input::Nibbler_Right },
	{ SDLK_LEFT, Input::Nibbler_Left },

	{ SDLK_a, Input::Nibbler_A },
	{ SDLK_w, Input::Nibbler_W },
	{ SDLK_d, Input::Nibbler_D },
	{ SDLK_s, Input::Nibbler_S },

	{ SDLK_r, Input::Nibbler_R },

	{ SDLK_1, Input::Nibbler_1 },
	{ SDLK_2, Input::Nibbler_2 },
	{ SDLK_3, Input::Nibbler_3 },

	{ SDLK_F1, Input::Nibbler_1 },
	{ SDLK_F2, Input::Nibbler_2 },
	{ SDLK_F3, Input::Nibbler_3 },
};

const int SDLRenderer::_DefaultBlockSize = 50;

const std::map<Block, std::function<void(const GameData& gameData, GUI*, std::shared_ptr<Background>)>> SDLRenderer::_TextureBlockFunction = {
	{
		Block::Food, [](const GameData&, GUI* gui, std::shared_ptr<Background> sprite) {
			sprite->setTexture(gui->fruitTexture());
		}
	},
	{
		Block::Bonus, [](const GameData&, GUI* gui, std::shared_ptr<Background> sprite) {
			sprite->setTexture(gui->bonusTexture());
		}
	},
	{
		Block::Wall, [](const GameData&, GUI* gui, std::shared_ptr<Background> sprite) {
			sprite->setTexture(gui->wallTexture());
		}
	},
	{
		Block::Snake_P1, [](const GameData&, GUI* gui, std::shared_ptr<Background> sprite) {
			sprite->setTexture(gui->snakeBodyTexture());
			sprite->setBackgroundColor(Colors::Snake_P1);
		}
	},
	{
		Block::Snake_Head_P1, [](const GameData& data, GUI* gui, std::shared_ptr<Background> sprite) {
			sprite->setTexture(gui->snakeHeadTexture());
			sprite->setBackgroundColor(Colors::Snake_P1);
			if (data.snake1Direction == Direction::South) {
				sprite->setAngle(90.0);
			} else if (data.snake1Direction == Direction::West) {
				sprite->setAngle(180.0);
			} else if (data.snake1Direction == Direction::North) {
				sprite->setAngle(270.0);
			}
		}
	},
	{
		Block::Snake_P2, [](const GameData&, GUI* gui, std::shared_ptr<Background> sprite) {
			sprite->setTexture(gui->snakeBodyTexture());
			sprite->setBackgroundColor(Colors::Snake_P2);
		}
	},
	{
		Block::Snake_Head_P2, [](const GameData& data, GUI* gui, std::shared_ptr<Background> sprite) {
			sprite->setTexture(gui->snakeHeadTexture());
			sprite->setBackgroundColor(Colors::Snake_P2);
			if (data.snake2Direction == Direction::South) {
				sprite->setAngle(90.0);
			} else if (data.snake2Direction == Direction::West) {
				sprite->setAngle(180.0);
			} else if (data.snake2Direction == Direction::North) {
				sprite->setAngle(270.0);
			}
		}
	},
};
