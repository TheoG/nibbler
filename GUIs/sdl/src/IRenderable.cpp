#include "IRenderable.hpp"
#include "Logger.hpp"

void IRenderable::setBorder(float, SDL_Color) {
}

void IRenderable::setBackgroundColor(SDL_Color color) {
	_color = color;
	if (_texture) {
		SDL_SetTextureColorMod(_texture, color.r, color.g, color.b);
	}
}

void IRenderable::setTexture(SDL_Texture* texture) {
	_texture = texture;
}
void IRenderable::setTexture(SDL_Renderer* renderer, const std::string& pathToTexture) {
	SDL_Surface* image;
	SDL_RWops* rwop;
	rwop = SDL_RWFromFile(pathToTexture.c_str(), "rb");
	image = IMG_LoadPNG_RW(rwop);
	if (!image) {
		Logger::Log << "Failed to load png (" << pathToTexture.c_str() << "): " <<  IMG_GetError() << "\n";
		return;
	}
	_texture = SDL_CreateTextureFromSurface(renderer, image);
	SDL_FreeSurface(image);
	SDL_RWclose(rwop);
}

void IRenderable::setSize(iVector2 size) {
	_dimensions.w = size.x;
	_dimensions.h = size.y;
}

void IRenderable::setPosition(iVector2 position) {
	_dimensions.x = position.x;
	_dimensions.y = position.y;
}

void IRenderable::setAngle(double angle) {
	_angle = angle;
}

void IRenderable::setEnabled(bool enabled) {
	_enabled = enabled;
}