#include "TextBox.hpp"

#include <iostream>

TextBox::TextBox(SDL_Rect dimensions, SDL_Color fontColor) {	
	_dimensions = dimensions;
	_centeredDimensions = dimensions;
	_text = "";
	_fontColor = fontColor;
	_enabled = true;
	_texture = nullptr;
	_surface = nullptr;
}

TextBox::~TextBox() {
	if (_surface) {
		SDL_FreeSurface(_surface);
	}
	if (_texture) {
		SDL_DestroyTexture(_texture);
	}

}

void TextBox::render(SDL_Renderer* renderer) {
	if (_enabled) {
		SDL_RenderCopy(renderer, _texture, nullptr, &_centeredDimensions);
	}
}

void TextBox::click(iVector2 mousePosition) {
	(void)mousePosition;
	return;
}

void TextBox::hover(iVector2 mousePosition) {
	(void)mousePosition;
}

void TextBox::setFontColor(SDL_Color color) {
	_fontColor = color;
}

void TextBox::setText(const std::string& text, SDL_Renderer* renderer, int fontSize, SDL_Color c, bool centered) {
	_fontColor = c;
	setText(text, renderer, fontSize, centered);
}

void TextBox::setText(const std::string& text, SDL_Renderer* renderer, int fontSize, bool centered) {
	auto it = TextBox::fonts.find(fontSize);
	if (it == TextBox::fonts.end()) {
		Logger::Log << "Init font first\n";
		return ;
	}
	_text = text;
	_surface = TTF_RenderText_Blended(it->second, _text.c_str(), _fontColor);
	if (!_surface) {
		std::cerr << "SDL Fatal error, exiting now..." << std::endl;
		std::exit(EXIT_FAILURE);
	}
	_dimensions.w = _surface->w;
	_dimensions.h = _surface->h;

	if (_texture) {
		SDL_DestroyTexture(_texture);
	}
	_texture = SDL_CreateTextureFromSurface(renderer, _surface);
	if (!_texture) {
		std::cerr << "SDL Fatal error, exiting now..." << std::endl;
		std::exit(EXIT_FAILURE);
	}
	TTF_SizeText(it->second, _text.c_str(), &_centeredDimensions.w, &_centeredDimensions.h); 
	if (centered) {
		_centeredDimensions.x = _dimensions.x - _centeredDimensions.w / 2;
		_centeredDimensions.y = _dimensions.y - _centeredDimensions.h / 2;
	}
	SDL_FreeSurface(_surface);
	_surface = nullptr;
}

void TextBox::loadFont() {
	auto font = TTF_OpenFont("resources/fonts/Chilanka-Regular.ttf", 25);
	if (font) {
		TextBox::fonts[25] = font;
	}
	font = TTF_OpenFont("resources/fonts/Chilanka-Regular.ttf", 50);
	if (font) {
		TextBox::fonts[50] = font;
	}
}

void TextBox::unloadFont() {
	for (auto& it : TextBox::fonts) {
		TTF_CloseFont(it.second);
	}
}

std::map<int, TTF_Font*> TextBox::fonts;

TextBox::TextBox() {}

TextBox::TextBox(const TextBox& other) {
	*this = other;
}

TextBox& TextBox::operator=(const TextBox& other) {
	_dimensions = other._dimensions;
	_text = other._text;
	return *this;
}