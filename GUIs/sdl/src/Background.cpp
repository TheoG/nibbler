#include "Background.hpp"

Background::Background(SDL_Rect dimensions, SDL_Color) {
	_dimensions = dimensions;
	_texture = nullptr;
	_angle = 0.0;

	_surface = SDL_CreateRGBSurface(0, dimensions.w, dimensions.h, 32, 0, 0, 0, 0);
	SDL_BlitSurface(nullptr, NULL, _surface, &_dimensions);
}

Background::~Background() {
	if (_surface) {
		SDL_FreeSurface(_surface);
	}
}

void Background::render(SDL_Renderer* renderer) {
	// SDL_RenderCopy(renderer, _texture, nullptr, &_dimensions);
	SDL_RenderCopyEx(renderer, _texture, nullptr, &_dimensions, _angle, nullptr, SDL_FLIP_NONE);
}

void Background::click(iVector2 mousePosition) {
	(void)mousePosition;
}

void Background::hover(iVector2 mousePosition) {
	(void)mousePosition;
}

Background::Background() {}

Background::Background(const Background& other) {
	*this = other;
}

Background& Background::operator=(const Background& other) {
	_dimensions = other._dimensions;
	_surface = other._surface;
	return *this;
}