#pragma once

#include "Background.hpp"
#include "TextBox.hpp"
#include "Vector.hpp"
#include "Colors.hpp"
#include "Types.hpp"

class GUI {

public:
	GUI();
	GUI(SDL_Renderer* renderer);
	~GUI();

	void setup(iVector2 screenSize);

	const std::vector<std::shared_ptr<IRenderable>>& getRenderables() const;

	void updateTimer(double elapsedTime);
	void updatePlayer1Score(int score, bool ready);
	void updatePlayer2Score(int score, bool ready);
	void updateVictoryMessage(WinState winner);
	std::shared_ptr<IRenderable> getVictoryText();

	SDL_Texture* wallTexture() const;
	SDL_Texture* fruitTexture() const;
	SDL_Texture* snakeBodyTexture() const;
	SDL_Texture* snakeHeadTexture() const;
	SDL_Texture* bonusTexture() const;
private:

	GUI(const GUI&);
	GUI& operator=(const GUI&);

	SDL_Renderer* _renderer;

	std::shared_ptr<Background> _background;
	std::shared_ptr<TextBox> _timer;
	std::shared_ptr<TextBox> _player1Score;
	std::shared_ptr<TextBox> _player2Score;
	std::shared_ptr<TextBox> _victoryMessage;

	SDL_Texture* _wallTexture;
	SDL_Texture* _fruitTexture;
	SDL_Texture* _snakeBodyTexture;
	SDL_Texture* _snakeHeadTexture;
	SDL_Texture* _bonusTexture;

	std::vector<std::shared_ptr<IRenderable>> _renderables;
};