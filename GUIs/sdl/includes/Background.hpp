#pragma once

#include "IRenderable.hpp"
#include "SDL.h"


class Background: public IRenderable {

public:
	Background(SDL_Rect dimensions, SDL_Color backgroundColor);
	~Background();

	void render(SDL_Renderer*);
	void click(iVector2 mousePosition);
	void hover(iVector2 mousePosition);
private:

	Background();
	Background(const Background&);
	Background& operator=(const Background&);
};