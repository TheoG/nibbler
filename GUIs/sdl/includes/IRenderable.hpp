#pragma once

#include <functional>
#include "Common.hpp"
#include "SDL.h"
#include "SDL2/SDL_image.h"

class IRenderable {

public:
	virtual void render(SDL_Renderer*) = 0;
	
	virtual void click(iVector2 mousePosition) = 0;
	virtual void hover(iVector2 mousePosition) = 0;

	std::vector<std::function<void(iVector2 mousePosition)>> clickCallbacks;
	std::vector<std::function<void(iVector2 mousePosition)>> hoverCallbacks;
	
	void setBorder(float thickness, SDL_Color color);
	void setBackgroundColor(SDL_Color color);

	void setTexture(SDL_Renderer* renderer, const std::string& pathToTexture);
	void setTexture(SDL_Texture* texture);

	void setSize(iVector2 size);
	void setPosition(iVector2 position);

	void setAngle(double angle);
	void setEnabled(bool);
protected:
	SDL_Surface* _surface;
	SDL_Rect _dimensions;
	SDL_Texture* _texture;
	SDL_Color _color;

	double _angle;
	bool _enabled;
};