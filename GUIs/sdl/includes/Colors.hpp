#pragma once

#include <SDL.h>

struct Colors {
	static const SDL_Color Transparent;
	static const SDL_Color White;
	static const SDL_Color Black;
	static const SDL_Color Snake_P1;
	static const SDL_Color Snake_P2;
	static const SDL_Color Timer;
};
