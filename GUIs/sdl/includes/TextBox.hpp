#pragma once

#include "IRenderable.hpp"
#include "Logger.hpp"
#include "SDL2/SDL_ttf.h"

class TextBox : public IRenderable {

public:
	TextBox(SDL_Rect dimensions, SDL_Color backgroundColor);
	~TextBox();

	void render(SDL_Renderer*);
	void click(iVector2 mousePosition);
	void hover(iVector2 mousePosition);

	void setText(const std::string& text, SDL_Renderer* renderer, int fontSize, SDL_Color c, bool centered = true);
	void setText(const std::string& text, SDL_Renderer* renderer, int fontSize, bool centered = true);
	void setFontColor(SDL_Color color);

	static void loadFont();
	static void unloadFont();
	static std::map<int, TTF_Font*> fonts;

private:

	TextBox();
	TextBox(const TextBox&);
	TextBox& operator=(const TextBox&);

	SDL_Color _fontColor;
	std::string _text;
	SDL_Rect _centeredDimensions;
};
