#pragma once

#include <map>
#include "IRenderer.hpp"
#include "SDL.h"
#include "GUI.hpp"
#include "Vector.hpp"
#include "Types.hpp"
#include "Logger.hpp"
#include "Colors.hpp"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_ttf.h"


#ifdef WIN32
 # if defined DLL_EXPORT
   # define DECLDIR __declspec(dllexport)
 #else
  # define DECLDIR __declspec(dllimport)
 #endif
#endif

class SDLRenderer final: public IRenderer {

public:

	SDLRenderer(iVector2 gameAreaSize);
	~SDLRenderer();

	void init();
	void deinit();

	std::vector<Input> getEvents();
	void updateUI(const GameData&);

	void resize(iVector2 newSize);


private:

	SDLRenderer();
	SDLRenderer(const SDLRenderer&);
	SDLRenderer& operator=(const SDLRenderer&);

	SDL_Window* _window;
	SDL_Renderer* _renderer;
	static const std::map<SDL_Keycode, Input> _InputTranslationTable;
	static const int _DefaultBlockSize;
	
	static const std::map<Block, std::function<void(const GameData& gameData, GUI*, std::shared_ptr<Background>)>> _TextureBlockFunction;

	GUI* _gui;
	iVector2 _gameAreaSize;
	std::vector<std::shared_ptr<Background>> _blocksTextures;
	int _blockSize;
	bool _shouldResize;
};


extern "C" {
	#ifdef WIN32
	DECLDIR IRenderer* createSDLRenderer(iVector2 gameAreaSize);
	DECLDIR void deleteSDLRenderer(IRenderer* renderer);
	#else
	IRenderer* createSDLRenderer(iVector2 gameAreaSize);
	void deleteSDLRenderer(IRenderer* renderer);
	#endif
}
