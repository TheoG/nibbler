#pragma once

#include <ncurses.h>
#include <map>
#include "IRenderer.hpp"
#include "Logger.hpp"
#include "Vector.hpp"

#define PRINT_WIDTH	20
#define BOARD_HEIGHT	(PRINT_WIDTH + 2)
#define BOARD_WIDTH	(3 * PRINT_WIDTH + 3)
#define INFOS_WIDTH	(COLS - BOARD_WIDTH) % 20
#define INFOS_X		BOARD_WIDTH
#define KEY_ESC		27
#define SNAKE1 1
#define SNAKE2 2
#define FOOD 3
#define BONUS 4
#define WALL 5
#define P1 6
#define P2 7
#define LOSE 8

class NcursesRenderer final : public IRenderer {

public:

	NcursesRenderer(iVector2 boardSize);
	~NcursesRenderer();
	void init();
	void deinit();

	std::vector<Input> getEvents();
	void updateUI(const GameData&);
	void resize(iVector2 newSize);

private:

	NcursesRenderer();
	NcursesRenderer& operator=(const NcursesRenderer&);
	NcursesRenderer(const NcursesRenderer&);

	static void printHeadDirection(const Direction& direction, WINDOW *board, int x, int y);

	WINDOW* _board;
	WINDOW* _infos;
	iVector2 _boardSize;
	bool _resize;

	static const std::map<int, Input> _InputTranslationTable;
	static const std::map<Block, std::function<void(const GameData& data, WINDOW *board, int x, int y)>> _TextureBlockFunction;
};

extern "C" {
	IRenderer* createNcursesRenderer(iVector2 boardSize);
	void deleteNcursesRenderer(IRenderer* ncursesRenderer);
}
