#include "NcursesRenderer.hpp"
#include "Logger.hpp"

NcursesRenderer::NcursesRenderer(iVector2 boardSize) : _boardSize(boardSize) {
	Logger::Log << "Ncurses Renderer Constructor\n";
}

NcursesRenderer::~NcursesRenderer() {
	Logger::Log << "Ncurses Renderer Desctructor\n";
}

NcursesRenderer::NcursesRenderer() {}

NcursesRenderer::NcursesRenderer(const NcursesRenderer& other) {
	*this = other;
}

NcursesRenderer& NcursesRenderer::operator=(const NcursesRenderer&) {
	return *this;
}

void NcursesRenderer::init() {
	Logger::setFileLog(true);
	Logger::Log << "NcursesRenderer::init\n";

	initscr();
	start_color();
	keypad(stdscr, TRUE);
	noecho();
	cbreak();
	curs_set(0);
	nodelay(stdscr, 1);

	_infos = subwin(stdscr, 3, 3 * _boardSize.x, 0, 0);
	_board = subwin(stdscr, _boardSize.y, 3 * _boardSize.x, 3, 0);

	init_pair(SNAKE1, COLOR_BLACK, COLOR_GREEN);
	init_pair(SNAKE2, COLOR_BLACK, COLOR_BLUE);
	init_pair(FOOD, COLOR_RED, COLOR_RED);
	init_pair(BONUS, COLOR_BLACK, COLOR_YELLOW);
	init_pair(WALL, COLOR_WHITE, COLOR_WHITE);
	init_pair(P1, COLOR_GREEN, COLOR_BLACK);
	init_pair(P2, COLOR_BLUE, COLOR_BLACK);
	init_pair(LOSE, COLOR_RED, COLOR_BLACK);
}

void NcursesRenderer::deinit() {
	Logger::Log << "NcursesRenderer::deinit\n";

	// deinit windows
	delwin(_board);
	delwin(_infos);
	endwin();
}

std::vector<Input> NcursesRenderer::getEvents() {
	std::vector<Input> inputs;
	int input;

	input = getch();

	auto it = _InputTranslationTable.find(input);
	if (it != _InputTranslationTable.end()) {
		inputs.push_back(it->second);
	}

	return inputs;
}

void NcursesRenderer::updateUI(const GameData& data) {
	if (_resize) {
		_resize = false;
		delwin(_board);
		delwin(_infos);
		_infos = subwin(stdscr, 3, 3 * _boardSize.x, 0, 0);
		_board = subwin(stdscr, _boardSize.y, 3 * _boardSize.x, 3, 0);
	}
	werase(_board);
	werase(_infos);
	wattron(_board, COLOR_PAIR(WALL));
	box(_board, ACS_VLINE, ACS_HLINE);
	wattroff(_board, COLOR_PAIR(WALL));
	for (int y = 0; y < _boardSize.y; y++) {
		for (int x = 0; x < _boardSize.x; x++) {
			Block b = data.blocks[y * _boardSize.x + x];
			auto it = _TextureBlockFunction.find(b);

			if (it != _TextureBlockFunction.end()) {
				it->second(data, _board, x, y);
			} else {
				continue;
			}
		}
	}

	mvwprintw(_infos, 0, (3 * _boardSize.x - 5) / 2, "%02d:%02d", static_cast<int>(data.elapsedTime / 60), static_cast<int>(data.elapsedTime) % 60);
	wattron(_infos, COLOR_PAIR(P1));
	if (data.player1Ready)
		wattron(_infos, A_BOLD);
	mvwprintw(_infos, 2, 0, "%04d", data.player1Score);
	wattroff(_infos, COLOR_PAIR(P1));
	if (data.player1Ready)
		wattroff(_infos, A_BOLD);
	if (data.player2Score > -1) {
		wattron(_infos, COLOR_PAIR(P2));
		if (data.player2Ready)
			wattron(_infos, A_BOLD);
		mvwprintw(_infos, 2, 3 * _boardSize.x - 4, "%04d", data.player2Score);
		wattroff(_infos, COLOR_PAIR(P2));
		if (data.player2Ready)
			wattroff(_infos, A_BOLD);
	}

	if (data.winner == WinState::Win) {
		wattron(_infos, COLOR_PAIR(P1));
		mvwprintw(_infos, 1, (3 * _boardSize.x - 8) / 2, "You win!");
		wattroff(_infos, COLOR_PAIR(P1));
	} else if (data.winner == WinState::Lose) {
		wattron(_infos, COLOR_PAIR(LOSE));
		mvwprintw(_infos, 1, (3 * _boardSize.x - 12) / 2, "You lose...!");
		wattroff(_infos, COLOR_PAIR(LOSE));
	} else if (data.winner == WinState::Player1) {
		wattron(_infos, COLOR_PAIR(P1));
		mvwprintw(_infos, 1, (3 * _boardSize.x - 13) / 2, "Player 1 win!");
		wattroff(_infos, COLOR_PAIR(P1));
	} else if (data.winner == WinState::Player2) {
		wattron(_infos, COLOR_PAIR(P2));
		mvwprintw(_infos, 1, (3 * _boardSize.x - 13) / 2, "Player 2 win!");
		wattroff(_infos, COLOR_PAIR(P2));
	} else if (data.winner == WinState::Draw) {
		mvwprintw(_infos, 1, (3 * _boardSize.x - 7) / 2, "Draw...");
	}

	wrefresh(_board);
	wrefresh(_infos);
}

void NcursesRenderer::resize(iVector2 newSize) {
	_resize = true;
	_boardSize = newSize;
}

void NcursesRenderer::printHeadDirection(const Direction& direction, WINDOW *board, int x, int y) {
	if (direction == North)
		mvwprintw(board, y, x * 3, "' '");
	else if (direction == South)
		mvwprintw(board, y, x * 3, ". .");
	else if (direction == East)
		mvwprintw(board, y, x * 3, "  :");
	else if (direction == West)
		mvwprintw(board, y, x * 3, ":  ");
}

extern "C" {
	IRenderer* createNcursesRenderer(iVector2 boardSize) {
		return new NcursesRenderer(boardSize);
	}

	void deleteNcursesRenderer(IRenderer* ncursesRenderer) {
		delete reinterpret_cast<NcursesRenderer*>(ncursesRenderer);
	}
}

const std::map<int, Input> NcursesRenderer::_InputTranslationTable = {
	{ KEY_ESC, Input::Nibbler_Escape },

	{ KEY_UP, Input::Nibbler_Up },
	{ KEY_DOWN, Input::Nibbler_Down },
	{ KEY_LEFT, Input::Nibbler_Left },
	{ KEY_RIGHT, Input::Nibbler_Right },

	{ 'w', Input::Nibbler_W },
	{ 's', Input::Nibbler_S },
	{ 'a', Input::Nibbler_A },
	{ 'd', Input::Nibbler_D },

	{ KEY_F(1), Input::Nibbler_1 },
	{ KEY_F(2), Input::Nibbler_2 },
	{ KEY_F(3), Input::Nibbler_3 },

	{ '1', Input::Nibbler_1 },
	{ '2', Input::Nibbler_2 },
	{ '3', Input::Nibbler_3 },

	{ 'r', Input::Nibbler_R },
	{ ' ', Input::Nibbler_Space }
};

const std::map<Block, std::function<void(const GameData& data, WINDOW *board, int x, int y)>> NcursesRenderer::_TextureBlockFunction = {
	{
		Block::Wall, [](const GameData&, WINDOW *board, int x, int y) {
			wattron(board, COLOR_PAIR(WALL));
			mvwprintw(board, y, x * 3, "   ");
			wattroff(board, COLOR_PAIR(WALL));
		}
	},
	{
		Block::Snake_P1, [](const GameData&, WINDOW *board, int x, int y) {
			wattron(board, COLOR_PAIR(SNAKE1));
			mvwprintw(board, y, x * 3, "   ");
			wattroff(board, COLOR_PAIR(SNAKE1));
		}
	},
	{
		Block::Snake_P2, [](const GameData&, WINDOW *board, int x, int y) {
			wattron(board, COLOR_PAIR(SNAKE2));
			mvwprintw(board, y, x * 3, "   ");
			wattroff(board, COLOR_PAIR(SNAKE2));
		}
	},
	{
		Block::Snake_Head_P1, [](const GameData& data, WINDOW *board, int x, int y) {
			wattron(board, COLOR_PAIR(SNAKE1));
			printHeadDirection(data.snake1Direction, board, x, y);
			wattroff(board, COLOR_PAIR(WALL));
		}
	},
	{
		Block::Snake_Head_P2, [](const GameData& data, WINDOW *board, int x, int y) {
			wattron(board, COLOR_PAIR(SNAKE2));
			printHeadDirection(data.snake2Direction, board, x, y);
			wattroff(board, COLOR_PAIR(SNAKE2));
		}
	},
	{
		Block::Food, [](const GameData&, WINDOW *board, int x, int y) {
			wattron(board, COLOR_PAIR(FOOD));
			mvwprintw(board, y, x * 3, "   ");
			wattroff(board, COLOR_PAIR(FOOD));
		}
	},
	{
		Block::Bonus, [](const GameData&, WINDOW *board, int x, int y) {
			wattron(board, COLOR_PAIR(BONUS));
			mvwprintw(board, y, x * 3, " ? ");
			wattroff(board, COLOR_PAIR(BONUS));
		}
	},
};
